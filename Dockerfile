FROM node:18-alpine as builder
ARG VITE_API_URL
ARG VITE_FILE_GET_URL
ARG VITE_PASSPHASE
WORKDIR /next/app
ENV VITE_API_URL=$VITE_API_URL
ENV VITE_FILE_GET_URL=$VITE_FILE_GET_URL
ENV VITE_PASSPHASE=$VITE_PASSPHASE
COPY package.json ./
COPY yarn.lock ./
RUN yarn
COPY . .
RUN yarn build

# production environment
FROM nginx:stable-alpine
COPY --from=builder /next/app/dist /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx.conf /etc/nginx/conf.d
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
