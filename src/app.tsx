import { Button } from "antd";
import { FC } from "react";

const App: FC = () => {
  return (
    <div className="App">
      <Button type="primary">Button</Button>
    </div>
  );
};

export default App;
