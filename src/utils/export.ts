/* eslint @typescript-eslint/no-explicit-any: off */
import dayjs from "dayjs";
import FileSaver from "file-saver";
import { WorkBook, utils, write } from "xlsx";

type ExportType<T> = (
  dataSource?: T[],
  label?: string,
  cols?: { idx: number; code: string }[]
) => Promise<void>;
type ExportRendderedJsonType = (
  dataSource?: readonly any[],
  children?: any[]
) => Promise<any[]>;
type ExportRendderedArrayType = (
  dataSource?: readonly any[],
  children?: any[]
) => Promise<[any[], string[]]>;

const functionName = (fun: any) => {
  let ret = fun.toString();
  ret = ret.substr("function ".length);
  ret = ret.substr(0, ret.indexOf("("));
  return ret;
};

const findRender = (childItem: any, item: any, itemInd: number) => {
  if (typeof childItem.render === "function") {
    if (
      childItem.dataIndex &&
      !childItem.dataIndex.includes("id") &&
      item[childItem.dataIndex] &&
      typeof item[childItem.dataIndex] === "number"
    ) {
      return item[childItem.dataIndex];
    }
    const rendered = childItem.render(item[childItem.dataIndex], item, itemInd);
    if (rendered && typeof rendered === "object") {
      if (rendered.props.status !== undefined) {
        return rendered.props.status ? "True" : "False";
      }
      if (rendered.props.label !== undefined) {
        return rendered.props.label;
      }
      if (
        Array.isArray(rendered.props.children) &&
        rendered.props.children.length > 0
      ) {
        if (["number", "string"].includes(typeof rendered.props.children[0]))
          return rendered.props.children.join("");
        if (
          typeof rendered.props.children[0]?.props?.children === "object" &&
          rendered.props.children[0]?.props?.children.length > 0
        ) {
          return rendered.props.children
            .map((_item: any) => _item.props.children, "")
            .join(", ");
        }
        if (typeof rendered.props.children[0]?.props?.children === "string") {
          return rendered.props.children
            .map((_item: any) => _item.props.children, "")
            .join(", ");
        }
        return rendered.props.children.map((_g: any) => {
          if (_g?.props?.label !== undefined) return _g.props.label;
          if (_g?.props?.status !== undefined)
            return rendered.props.status ? "True" : "False";
          return _g;
        });
      }
      return rendered.props.children;
    }
    return rendered;
  }
  return item[childItem.dataIndex];
};

export const exportRenderedJson: ExportRendderedJsonType = (
  dataSource = [],
  children = []
) =>
  new Promise((resolve, reject) => {
    try {
      resolve(
        (dataSource || []).map((item, itemInd) =>
          children.reduce((acc, child, index) => {
            const getTitle = (_child: any) => {
              if (typeof _child.key === "string") return _child.key;
              return _child.props.title.props.children[0];
            };
            if (index === 0) {
              acc["№"] = itemInd + 1;
            }
            if (functionName(child.type) === "ColumnGroup") {
              child.props.children.map((_cd: any) => {
                acc[`${child.key}-${getTitle(_cd)}`] = findRender(
                  _cd.props,
                  item,
                  itemInd
                );
                return null;
              });
            } else {
              acc[getTitle(child)] = findRender(child.props, item, itemInd);
            }
            return acc;
          }, {})
        )
      );
    } catch (error) {
      reject(error);
    }
  });

export const exportRenderedArray: ExportRendderedArrayType = (
  dataSource = [],
  children = []
) =>
  new Promise((resolve, reject) => {
    try {
      const headers = children.reduce((acc, child) => {
        if (functionName(child.type) === "ColumnGroup") {
          child.props.children.map((_cd: any) => {
            acc.push(
              `${child.props.title}-${_cd.props.title.props.children[0]}`
            );
            return null;
          });
        } else if (typeof child.props.title === "string") {
          acc.push(child.props.title);
        } else {
          acc.push(child.props.title.props.children[0]);
        }
        return acc;
      }, []);
      const rendered = (dataSource || []).map((item, itemInd) =>
        children.reduce((acc, child) => {
          if (functionName(child.type) === "ColumnGroup") {
            child.props.children.map((_cd: any) => {
              acc.push(findRender(_cd.props, item, itemInd));
              return null;
            });
          } else {
            acc.push(findRender(child.props, item, itemInd));
          }
          return acc;
        }, [])
      );
      resolve([rendered, headers]);
    } catch (error) {
      reject(error);
    }
  });

const getName = (obj?: any) => {
  if ("name" in obj) {
    return obj.name;
  }
  if ("first_name" in obj && "last_name" in obj) {
    return `${obj.first_name} ${obj.last_name}`;
  }
  return null;
};

export const exportXLSX: ExportType<any> = async (
  dataSource = [],
  label = "Podcast",
  cols = []
) =>
  new Promise((resolve, reject) => {
    const realData = [];
    // eslint-disable-next-line no-restricted-syntax
    for (const obj of dataSource) {
      let one = {};
      Object.entries(obj).forEach(([key, value]) => {
        if (
          key.search("_path") !== -1 ||
          key.search("_id") !== -1 ||
          key.search("_uid") !== -1 ||
          key === "id"
        ) {
          return;
        }
        if (typeof value !== "object") {
          one = {
            ...one,
            [key]: value,
          };
        } else if (value && typeof value === "object") {
          one = {
            ...one,
            [`${key}_name`]: getName(value),
          };
        }
      });
      realData.push(one);
    }
    dataSource = realData;
    // console.log("realData", realData);
    try {
      const ws = utils.json_to_sheet(dataSource);
      const wb = { Sheets: { data: ws }, SheetNames: ["data"] };
      if (typeof cols === "object" && cols.length > 0) {
        addFormula(wb, dataSource, cols);
      }
      // console.log("ws", ws);
      const excelBuffer = write(wb, { bookType: "xlsx", type: "array" });
      const data = new Blob([excelBuffer], {
        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8",
      });
      FileSaver.saveAs(
        data,
        `${`${label}-${dayjs().format("YYYY-MM-DD")}`}.xlsx`
      );
      resolve();
    } catch (error) {
      reject(error);
    }
  });

const addFormula = (
  wb: WorkBook,
  dataSource: any[],
  cols: { idx: number; code: string }[]
) => {
  const firstSheetName = wb.SheetNames[0];
  const sheet = wb.Sheets[firstSheetName];
  cols.forEach((e) => {
    const cellRef = utils.encode_cell({ c: e.idx, r: dataSource.length + 1 });
    const cell = sheet[cellRef];
    if (!cell) {
      // add new cell
      utils.sheet_add_aoa(
        sheet,
        [[{ t: "n", f: `SUM(${e.code}2:${e.code}${dataSource.length + 1})` }]],
        {
          origin: cellRef,
        }
      );
    }
  });
};
