import dayjs from "dayjs";

export const getDateNDaysBefore = (days: number): Date => {
  const date = new Date(new Date().toDateString());
  date.setDate(date.getDate() - days + 1);
  return date;
};

export const calcDiffInSecund = (
  date1: string | Date,
  date2: string | Date
) => {
  return dayjs(date2).diff(date1, "second", false);
};

export function renderSecondsToHHMMSS(seconds: number) {
  // Ensure seconds is a non-negative number
  if (typeof seconds !== "number" || seconds < 0) {
    throw new Error(
      "Invalid input. Please provide a non-negative number of seconds."
    );
  }

  // Calculate hours, minutes, and remaining seconds
  const hours = Math.floor(seconds / 3600);
  const minutes = Math.floor((seconds % 3600) / 60);
  const remainingSeconds = Math.floor(seconds % 60);

  // Format the result as HH:MM:SS
  const formattedTime = `${padZero(hours)}:${padZero(minutes)}:${padZero(
    remainingSeconds
  )}`;
  return formattedTime;
}

// Helper function to pad a single digit number with a leading zero
function padZero(num: number) {
  return num < 10 ? `0${num}` : `${num}`;
}
export const FormatDDHH = "DDө HHц";
export const FormatMMDD = "Mс DDө";
export const FormatYYYYMM = "YYYYо Mс";
