import dayjs from "dayjs";
import { HTMLAttributes } from "react";

export const isEmptyDate: (date?: string | null) => boolean = (date) => {
  if (!date) return true;
  return date.startsWith("0001-01-01");
};

export const waitTime = (time: number = 100) => {
  return new Promise<boolean>((resolve) => {
    setTimeout(() => {
      resolve(true);
    }, time);
  });
};

export const getStr = (id?: number) => {
  if (!id) return "0";
  return id.toString();
};

export const isImage = (ext: string): boolean =>
  ext.search(/.(jpg|jpeg|png|gif)$/i) >= 0;

export const renderDate: (
  date?: Date | null | string,
  time?: boolean
) => string = (date, time = false) => {
  if (!date) return "-";
  return isEmptyDate(date?.toString())
    ? "-"
    : dayjs(date).format(time ? "YYYY-MM-DD HH:mm:ss" : "YYYY-MM-DD");
};

export const renderTime: (date?: Date | null | string) => string = (date) => {
  if (!date) return "-";
  if (typeof date === "string" && date.length == 5) {
    return date;
  }
  return isEmptyDate(date?.toString()) ? "-" : dayjs(date).format("HH:mm");
};

export const formatAPIDate = (date?: any): string => {
  if (date) return dayjs(date).format("YYYY-MM-DDTHH:mm:ssZ");
  return "";
};

export const renderMoney: (money?: number | null) => string = (money) => {
  if (!money) return "0";
  return new Intl.NumberFormat("en", {
    maximumFractionDigits: 2,
  }).format(money);
};

export const tableCellFixed: (width: number) => {
  width: number;
  onCell: () => HTMLAttributes<HTMLElement>;
} = (width) => ({
  width,
  onCell: () => ({ style: { maxWidth: width, minWidth: width } }),
});

export const renderFullname = (obj?: any) => {
  if (obj && "first_name" in obj && "last_name" in obj) {
    return (
      obj["last_name"]?.substr(0, 1).toUpperCase() + ". " + obj["first_name"]
    );
  }
};

export const renderNameInitial = (obj?: any) => {
  if (obj && "first_name" in obj && "last_name" in obj) {
    return (
      obj["last_name"]?.substr(0, 1).toUpperCase() +
      "" +
      obj["first_name"]?.substr(0, 1).toUpperCase()
    );
  }
};
export const renderOrgInitial = (obj?: any) => {
  if (obj && "name" in obj) {
    return obj["name"]?.substr(0, 1).toUpperCase();
  }
};
export const renderOrgFullName = (obj?: any, maxLength: number = 20) => {
  if (obj && "name" in obj) {
    const truncatedName =
      obj["name"].length > maxLength
        ? obj["name"].substring(0, maxLength) + "..."
        : obj["name"];

    return truncatedName + "  (" + obj["summary_name"] + ")";
  }
  return "";
};

export const tablePagination: (
  params: Partial<{
    pageSize?: number;
    current?: number;
    [x: string]: any;
  }>,
  sort: any
) => { limit: number; page: number; sorter: any; [x: string]: any } = (
  { pageSize, current, ...rest },
  sort
) => {
  return {
    limit: pageSize || 20,
    page: (current || 1) - 1,
    sorter: sort,
    ...rest,
  };
};

export const findFindByAttibute: <T extends any>(params: {
  code: keyof T;
  val: any;
  list: T[];
}) => T | null = ({ val, code, list }) => {
  const index = list.findIndex((item: any) => item[code] === val);
  if (index === -1) {
    return null;
  }
  return { ...(list[index] as any) };
};

export const renderPassengerType: (val: string) => string = (val: string) => {
  const mapToStr: any = {
    ADT: "Том хүн",
    CHD: "Хүүхэд",
    INF: "Няраа",
  };
  return mapToStr[val] ? mapToStr[val] : "-";
};

export const renderHHMM: (val?: string | null) => string = (val) => {
  if (!val) {
    return "-";
  }

  return `${val.substr(0, 2)}:${val.substr(2, 2)}`;
};

export const extractHHMM: (val?: string | number | null) => number[] = (
  val
) => {
  if (!val) {
    return [0, 0];
  }
  if (typeof val === "string") {
    val = parseInt(val);
  }

  if (val <= 0) {
    return [0, 0];
  }

  return [Math.floor(val / 60), Math.floor(val % 60)];
};

export const renderDateDDMMYY: (date: any) => string = (date) => {
  if (!date) return "-";
  return dayjs(date, "DDMMYY").format("YYYY-MM-DD");
};

export const extractDateTimeDiffrenceInHHMM = (
  startDate: string,
  endDate: string
) => {
  const sDate = dayjs(startDate, "HHmm DDMMYY");
  const eDate = dayjs(endDate, "HHmm DDMMYY");
  const diff = eDate.diff(sDate, "minute");
  return extractHHMM(diff);
};

export const getQueryVariable = (variable: string) => {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    if (pair[0] === variable) {
      return pair[1];
    }
  }
  return false;
};

export const renderBytes = (bytes: number, decimals = 2) => {
  if (!+bytes) return "0 Bytes";

  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = [
    "Bytes",
    "KiB",
    "MiB",
    "GiB",
    "TiB",
    "PiB",
    "EiB",
    "ZiB",
    "YiB",
  ];

  const i = Math.floor(Math.log(bytes) / Math.log(k));

  return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`;
};

export const convertMinutesToHours = (minutes?: number): string => {
  if (minutes === undefined) {
    return "0";
  }

  if (minutes < 100) return `${Math.round(minutes)} мин`;

  const hours = minutes / 60;
  const roundedHours = Math.round(hours * 100) / 100; // Rounds to two decimal places

  return `${roundedHours.toString()} цаг`;
};
