import dayjs from "dayjs";
import { exportXLSX } from "./export";

type tplotOptions = {
  [key: string]: any;
};

export const download = async (
  filename: string[],
  tableWrapper?: HTMLElement
): Promise<boolean> =>
  new Promise((resolve) => {
    if (!tableWrapper) {
      resolve(false);
      return;
    }

    const table = tableWrapper.querySelector("table") as HTMLTableElement;
    const keys: string[] = [];
    const rowArray = Array.from(table?.rows || []);
    Array.from(rowArray[0]?.cells).forEach((cell) => {
      keys.push(cell.innerText);
    });
    const maindata: tplotOptions[] = [];
    let i = 2;
    for (i; i < rowArray.length; i += 1) {
      const record: tplotOptions = {};
      const row = rowArray[i];
      const cells = Array.from(row.cells);
      let j = 0;
      for (j; j < cells.length; j += 1) {
        const cell = cells[j];
        record[keys[j]] = cell.innerText;
      }
      maindata.push(record);
    }

    let finalFileName = "";
    if (filename.length === 1) {
      finalFileName = `${`${filename[0]}-${dayjs().format(
        "YYYY-MM-DD"
      )}`}.xlsx`;
    } else {
      finalFileName = `${`${filename.join("-")}`}.xlsx`;
    }
    const filtered = [];
    for (const item of maindata) {
      const newItem: any = {};
      for (const key of Object.keys(item)) {
        if (key.includes("Зураг")) {
          continue;
        }
        newItem[key] = item[key];
      }
      filtered.push({ ...newItem });
    }
    exportXLSX(filtered, finalFileName);
    resolve(true);
  });
