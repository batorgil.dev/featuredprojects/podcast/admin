import { useLocation } from "react-router-dom";
import queryString from "query-string";

function useQuery<T>() {
  return queryString.parse(useLocation().search) as T;
}

export default useQuery;
