import { useEffect, useState } from "react";

const getWindowDimensions: (baseWidth?: number) => number = (
  baseWidth = 500
) => {
  if (typeof window !== "undefined") {
    const { innerWidth: width } = window;
    if (width > baseWidth) return baseWidth;
    return width;
  }
  return 0;
};

const useDrawerWidth: (baseWidth?: number) => number = (baseWidth = 500) => {
  const [width, setWidth] = useState<number>(getWindowDimensions(baseWidth));

  useEffect(() => {
    const handleResize = () => {
      setWidth(getWindowDimensions(baseWidth));
    };
    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, [baseWidth]);

  return width;
};

export default useDrawerWidth;
