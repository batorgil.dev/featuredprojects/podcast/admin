export const deleteConfirm = "delete me";
export const deleteConfirmReg = /^delete me$/g;
export const archiveConfirm = "archive me";
export const archiveConfirmReg = /^archive me$/g;
export const restoreConfirm = "restore me";
export const restoreConfirmReg = /^restore me$/g;
export const acceptConfirm = "accept me";
export const acceptConfirmReg = /^accept me$/g;
