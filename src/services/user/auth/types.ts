import { AntdFile } from "types";
import { User } from "../user/types";

export interface LoginData {
  email: string;
  password: string;
  remember: boolean;
}

export interface ChangePasswordInput {
  old_password: string;
  password: string;
}

export interface InfoUpdateInput {
  first_name: string;
  last_name: string;
  avatar_path: string;
  email: string;
  bio: string;
  social_links: any[];
  avatar?: AntdFile[];
}

export interface LoginResponse {
  // email?: string;
  token: string;
  user?: User;
}
