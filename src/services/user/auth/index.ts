import { decryptWithAES, encryptWithAES } from "utils/parse";
import http from "../../index";
import {
  LoginData,
  LoginResponse,
  ChangePasswordInput,
  InfoUpdateInput,
} from "./types";
import { SuccessResponse } from "types";
import { User } from "../user/types";

namespace auth {
  const userKey = "app.user";
  const tokenKey = "app.token";

  export const saveToken = (token: string) => {
    localStorage.setItem(tokenKey, token);
  };

  export const rememberUser = (values: LoginData) => {
    if (values.remember) {
      localStorage.setItem(userKey, encryptWithAES(JSON.stringify(values)));
    } else {
      localStorage.removeItem(userKey);
    }
  };

  export const getRememberUser = () => {
    const userData = localStorage.getItem(userKey);
    if (userData) {
      try {
        const _userData = JSON.parse(decryptWithAES(userData)) as LoginData;
        return _userData;
      } catch (err) {
        console.error(err);
        return undefined;
      }
    }
    return undefined;
  };

  export const remToken = () => {
    localStorage.removeItem(tokenKey);
  };

  export const hasToken = () => !!localStorage.getItem(tokenKey);

  export const getToken = () => localStorage.getItem(tokenKey);

  export const info = () =>
    http.get<User>("/user/auth/info", { hasAuth: true });

  export const login = (body: LoginData) =>
    http.post<LoginResponse>("user/auth/login", {
      body,
    });

  export const changePassword = (body: ChangePasswordInput) =>
    http.post<SuccessResponse>("user/auth/password_change", {
      body,
      hasAuth: true,
    });

  export const update = (body: InfoUpdateInput) =>
    http.post<SuccessResponse>("user/auth/update", {
      body,
      hasAuth: true,
    });

  export const tokenRefresh = () =>
    http.get<LoginResponse>("user/auth/token_refresh", {
      hasAuth: true,
    });
}

export default auth;
