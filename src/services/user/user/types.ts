import { AntdFile, Base, PaginationRequest } from "types";
import { SocialProvider } from "../settings/social_provider/types";

export interface UserFilterInput extends PaginationRequest {
  first_name?: string;
  last_name?: string;
  email?: string;
  position?: string;
  bio?: string;
  is_active?: string;
  role?: string;
  created_at?: string[];
}

export const UserRoleEnum = {
  admin: { text: "Admin" },
  user: { text: "User" },
};

export interface SocialLink {
  url: string;
  user_id: number;
  social_provider_id: number;
  social_provider?: SocialProvider;
}

export interface UserCreateInput {
  first_name: string;
  last_name: string;
  avatar_path: string;
  email: string;
  position: string;
  bio: string;
  social_links: SocialLink[];
  password: string;
  is_active: boolean;
  role: "admin" | "user";
  image?: AntdFile[];
}

export interface UserUpdateInput {
  first_name: string;
  last_name: string;
  avatar_path: string;
  email: string;
  position: string;
  bio: string;
  social_links: SocialLink[];
  is_active: boolean;
  role: "admin" | "user";
  image?: AntdFile[];
}

export interface ChangePasswordAdminInput {
  password: string;
  confirm?: string;
}

export interface User extends Base {
  first_name: string;
  last_name: string;
  avatar_path: string;
  email: string;
  position: string;
  bio: string;
  is_active: boolean;
  role: "admin" | "user";
  social_links: SocialLink[];
}
