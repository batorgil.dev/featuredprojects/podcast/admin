import http from "services";
import { PaginationResponse, SuccessResponse } from "types";
import {
  ChangePasswordAdminInput,
  User,
  UserCreateInput,
  UserFilterInput,
  UserUpdateInput,
} from "./types";

namespace user {
  export const page = async (body: UserFilterInput) =>
    http.post<PaginationResponse<User>>(`/user/user/page`, {
      body,
      hasAuth: true,
    });

  export const create = async (body: UserCreateInput) =>
    http.post<SuccessResponse>(`/user/user/create`, {
      body,
      hasAuth: true,
    });

  export const update = async (id: string | number, body: UserUpdateInput) =>
    http.put<SuccessResponse>(`/user/user/update/${id}`, {
      body,
      hasAuth: true,
    });

  export const changePassword = async (
    id: string | number,
    body: ChangePasswordAdminInput
  ) =>
    http.post<SuccessResponse>(`/user/user/change_password/${id}`, {
      body,
      hasAuth: true,
    });

  export const remove = async (id: string | number) =>
    http.del<SuccessResponse>(`/user/user/delete/${id}`, {
      hasAuth: true,
    });
}

export default user;
