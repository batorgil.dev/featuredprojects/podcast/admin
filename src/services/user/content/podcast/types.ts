import { Category } from "services/user/settings/category/types";
import { PodcastProvider } from "services/user/settings/podcast_provider/types";
import { User } from "services/user/user/types";
import { AntdFile, Base, PaginationRequest } from "types";

export interface PodcastFilterInput extends PaginationRequest {
  title?: string;
  summary?: string;
  description_html?: string;
  is_featured?: string;
  status?: "draft" | "published";
  author_id?: number;
  category_ids?: number[];
  created_at?: string[];
  published_at?: string[];
}

export interface PodcastCreateInput {
  title: string;
  summary: string;
  description_html: any;
  is_featured: boolean;
  cover_path: string;
  audio_path: string;
  category_ids?: number[];
  categories: { id: number }[];
  listen_ons: { podcast_provider_id: number; url: string }[];
  image?: AntdFile[];
  audio?: AntdFile[];
}

export interface PodcastUpdateInput {
  title: string;
  summary: string;
  description_html: any;
  is_featured: boolean;
  cover_path: string;
  audio_path: string;
  category_ids?: number[];
  categories: { id: number }[];
  listen_ons: { podcast_provider_id: number; url: string }[];
  image?: AntdFile[];
  audio?: AntdFile[];
}

export interface ProviderMap extends Base {
  url: string;
  podcast_id: number;
  podcast_provider_id: number;
  podcast_provider?: PodcastProvider;
}

export interface Podcast extends Base {
  title: string;
  summary: string;
  description_html: string;
  is_featured: boolean;
  cover_path: string;
  audio_path: string;
  view_count: number;
  author_id: number;
  author?: User;
  categories?: Category[];
  listen_ons?: ProviderMap[];
  status: "draft" | "published";
  published_at: string;
}
