import http from "services";
import {
  Podcast,
  PodcastCreateInput,
  PodcastFilterInput,
  PodcastUpdateInput,
} from "./types";
import { PaginationResponse, SuccessResponse } from "types";

namespace podcast {
  export const page = async (body: PodcastFilterInput) =>
    http.post<PaginationResponse<Podcast>>(`/user/content/podcast/page`, {
      body,
      hasAuth: true,
    });

  export const get = async (id: number | string) =>
    http.get<Podcast>(`/user/content/podcast/get/${id}`, {
      hasAuth: true,
    });

  export const create = async (body: PodcastCreateInput) =>
    http.post<SuccessResponse>(`/user/content/podcast/create`, {
      body,
      hasAuth: true,
    });

  export const update = async (id: string | number, body: PodcastUpdateInput) =>
    http.put<SuccessResponse>(`/user/content/podcast/update/${id}`, {
      body,
      hasAuth: true,
    });

  export const publish = async (id: string | number) =>
    http.put<SuccessResponse>(`/user/content/podcast/publish/${id}`, {
      hasAuth: true,
    });

  export const backToDraft = async (id: string | number) =>
    http.put<SuccessResponse>(`/user/content/podcast/back_to_draft/${id}`, {
      hasAuth: true,
    });

  export const remove = async (id: string | number) =>
    http.del<SuccessResponse>(`/user/content/podcast/delete/${id}`, {
      hasAuth: true,
    });
}

export default podcast;
