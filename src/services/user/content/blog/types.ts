import { Category } from "services/user/settings/category/types";
import { User } from "services/user/user/types";
import { AntdFile, Base, PaginationRequest } from "types";

export interface BlogFilterInput extends PaginationRequest {
  title?: string;
  status?: "draft" | "published";
  summary?: string;
  description_html?: string;
  is_featured?: string;
  author_id?: number;
  category_ids?: number[];
  created_at?: string[];
  published_at?: string[];
}

export interface BlogCreateInput {
  title: string;
  summary: string;
  description_html: any;
  is_featured: boolean;
  cover_path: string;
  categories: { id: number }[];
  category_ids?: number[];
  image?: AntdFile[];
}

export interface BlogUpdateInput {
  title: string;
  summary: string;
  description_html: any;
  is_featured: boolean;
  cover_path: string;
  category_ids?: number[];
  categories: { id: number }[];
  image?: AntdFile[];
}

export interface Blog extends Base {
  title: string;
  summary: string;
  description_html: string;
  is_featured: boolean;
  cover_path: string;
  view_count: number;
  status: "draft" | "published";
  published_at: string;
  author_id: number;
  author?: User;
  categories?: Category[];
}

export const ContentStatusEnum = {
  draft: {
    text: "Draft",
    color: "red",
  },
  published: {
    text: "Published",
    color: "green",
  },
};
