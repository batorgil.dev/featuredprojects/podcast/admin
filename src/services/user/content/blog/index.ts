import http from "services";
import {
  Blog,
  BlogCreateInput,
  BlogFilterInput,
  BlogUpdateInput,
} from "./types";
import { PaginationResponse, SuccessResponse } from "types";

namespace blog {
  export const page = async (body: BlogFilterInput) =>
    http.post<PaginationResponse<Blog>>(`/user/content/blog/page`, {
      body,
      hasAuth: true,
    });

  export const get = async (id: number | string) =>
    http.get<Blog>(`/user/content/blog/get/${id}`, {
      hasAuth: true,
    });

  export const create = async (body: BlogCreateInput) =>
    http.post<SuccessResponse>(`/user/content/blog/create`, {
      body,
      hasAuth: true,
    });

  export const update = async (id: string | number, body: BlogUpdateInput) =>
    http.put<SuccessResponse>(`/user/content/blog/update/${id}`, {
      body,
      hasAuth: true,
    });

  export const publish = async (id: string | number) =>
    http.put<SuccessResponse>(`/user/content/blog/publish/${id}`, {
      hasAuth: true,
    });

  export const backToDraft = async (id: string | number) =>
    http.put<SuccessResponse>(`/user/content/blog/back_to_draft/${id}`, {
      hasAuth: true,
    });

  export const remove = async (id: string | number) =>
    http.del<SuccessResponse>(`/user/content/blog/delete/${id}`, {
      hasAuth: true,
    });
}

export default blog;
