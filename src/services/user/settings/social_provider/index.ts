import http from "services";
import {
  SocialProvider,
  SocialProviderCreateInput,
  SocialProviderFilterInput,
  SocialProviderUpdateInput,
} from "./types";
import { PaginationResponse, SuccessResponse } from "types";

namespace socialProvider {
  export const page = async (body: SocialProviderFilterInput) =>
    http.post<PaginationResponse<SocialProvider>>(
      `/user/settings/social_provider/page`,
      {
        body,
        hasAuth: true,
      }
    );

  export const create = async (body: SocialProviderCreateInput) =>
    http.post<SuccessResponse>(`/user/settings/social_provider/create`, {
      body,
      hasAuth: true,
    });

  export const update = async (
    id: string | number,
    body: SocialProviderUpdateInput
  ) =>
    http.put<SuccessResponse>(`/user/settings/social_provider/update/${id}`, {
      body,
      hasAuth: true,
    });

  export const remove = async (id: string | number) =>
    http.del<SuccessResponse>(`/user/settings/social_provider/delete/${id}`, {
      hasAuth: true,
    });
}

export default socialProvider;
