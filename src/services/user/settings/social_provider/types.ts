import { AntdFile, Base, PaginationRequest } from "types";

export interface SocialProviderFilterInput extends PaginationRequest {
  url?: string;
  name?: string;
  created_at?: string[];
}

export interface SocialProviderCreateInput {
  url: string;
  name: string;
  logo_path: string;
  image?: AntdFile[];
}

export interface SocialProviderUpdateInput {
  url: string;
  name: string;
  logo_path: string;
  image?: AntdFile[];
}

export interface SocialProvider extends Base {
  url: string;
  name: string;
  logo_path: string;
}
