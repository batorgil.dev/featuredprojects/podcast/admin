import { AntdFile, Base, PaginationRequest } from "types";

export interface PodcastProviderFilterInput extends PaginationRequest {
  url?: string;
  name?: string;
  created_at?: string[];
}

export interface PodcastProviderCreateInput {
  url: string;
  name: string;
  logo_path: string;
  image?: AntdFile[];
}

export interface PodcastProviderUpdateInput {
  url: string;
  name: string;
  logo_path: string;
  image?: AntdFile[];
}

export interface PodcastProvider extends Base {
  url: string;
  name: string;
  logo_path: string;
}
