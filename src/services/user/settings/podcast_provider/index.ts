import http from "services";
import {
  PodcastProvider,
  PodcastProviderCreateInput,
  PodcastProviderFilterInput,
  PodcastProviderUpdateInput,
} from "./types";
import { PaginationResponse, SuccessResponse } from "types";

namespace podcastProvider {
  export const page = async (body: PodcastProviderFilterInput) =>
    http.post<PaginationResponse<PodcastProvider>>(
      `/user/settings/podcast_provider/page`,
      {
        body,
        hasAuth: true,
      }
    );

  export const create = async (body: PodcastProviderCreateInput) =>
    http.post<SuccessResponse>(`/user/settings/podcast_provider/create`, {
      body,
      hasAuth: true,
    });

  export const update = async (
    id: string | number,
    body: PodcastProviderUpdateInput
  ) =>
    http.put<SuccessResponse>(`/user/settings/podcast_provider/update/${id}`, {
      body,
      hasAuth: true,
    });

  export const remove = async (id: string | number) =>
    http.del<SuccessResponse>(`/user/settings/podcast_provider/delete/${id}`, {
      hasAuth: true,
    });
}

export default podcastProvider;
