import { AntdFile, Base, PaginationRequest } from "types";

export interface SponserFilterInput extends PaginationRequest {
  website?: string;
  created_at?: string[];
}

export interface SponserCreateInput {
  website: string;
  logo_path: string;
  image?: AntdFile[];
}

export interface SponserUpdateInput {
  website: string;
  logo_path: string;
  image?: AntdFile[];
}

export interface Sponser extends Base {
  website: string;
  logo_path: string;
}
