import http from "services";
import {
  Sponser,
  SponserCreateInput,
  SponserFilterInput,
  SponserUpdateInput,
} from "./types";
import { PaginationResponse, SuccessResponse } from "types";

namespace sponser {
  export const page = async (body: SponserFilterInput) =>
    http.post<PaginationResponse<Sponser>>(`/user/settings/sponser/page`, {
      body,
      hasAuth: true,
    });

  export const create = async (body: SponserCreateInput) =>
    http.post<SuccessResponse>(`/user/settings/sponser/create`, {
      body,
      hasAuth: true,
    });

  export const update = async (id: string | number, body: SponserUpdateInput) =>
    http.put<SuccessResponse>(`/user/settings/sponser/update/${id}`, {
      body,
      hasAuth: true,
    });

  export const remove = async (id: string | number) =>
    http.del<SuccessResponse>(`/user/settings/sponser/delete/${id}`, {
      hasAuth: true,
    });
}

export default sponser;
