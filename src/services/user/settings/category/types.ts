import { Base, PaginationRequest } from "types";

export interface CategoryFilterInput extends PaginationRequest {
  name?: string;
  created_at?: string[];
}

export interface CategoryCreateInput {
  name: string;
}

export interface CategoryUpdateInput {
  name: string;
}

export interface Category extends Base {
  name: string;
}
