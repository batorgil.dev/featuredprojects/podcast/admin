import http from "services";
import {
  Category,
  CategoryCreateInput,
  CategoryFilterInput,
  CategoryUpdateInput,
} from "./types";
import { PaginationResponse, SuccessResponse } from "types";

namespace category {
  export const page = async (body: CategoryFilterInput) =>
    http.post<PaginationResponse<Category>>(`/user/settings/category/page`, {
      body,
      hasAuth: true,
    });

  export const create = async (body: CategoryCreateInput) =>
    http.post<SuccessResponse>(`/user/settings/category/create`, {
      body,
      hasAuth: true,
    });

  export const update = async (
    id: string | number,
    body: CategoryUpdateInput
  ) =>
    http.put<SuccessResponse>(`/user/settings/category/update/${id}`, {
      body,
      hasAuth: true,
    });

  export const remove = async (id: string | number) =>
    http.del<SuccessResponse>(`/user/settings/category/delete/${id}`, {
      hasAuth: true,
    });
}

export default category;
