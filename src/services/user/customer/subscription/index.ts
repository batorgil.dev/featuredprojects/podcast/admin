import http from "services";
import { Subscription, SubscriptionFilterInput } from "./types";
import { PaginationResponse, SuccessResponse } from "types";

namespace subscription {
  export const page = async (body: SubscriptionFilterInput) =>
    http.post<PaginationResponse<Subscription>>(
      `/user/customer/subscription/page`,
      {
        body,
        hasAuth: true,
      }
    );

  export const remove = async (id: string | number) =>
    http.del<SuccessResponse>(`/user/customer/subscription/delete/${id}`, {
      hasAuth: true,
    });
}

export default subscription;
