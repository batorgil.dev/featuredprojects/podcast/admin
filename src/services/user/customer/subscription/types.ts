import { Base, PaginationRequest } from "types";

export interface SubscriptionFilterInput extends PaginationRequest {
  email?: string;
  created_at?: string[];
}

export interface Subscription extends Base {
  email: string;
}
