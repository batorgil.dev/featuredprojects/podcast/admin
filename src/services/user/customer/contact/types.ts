import { Base, PaginationRequest } from "types";

export interface ContactFilterInput extends PaginationRequest {
  fullname?: string;
  email?: string;
  title?: string;
  message?: string;
  created_at?: string[];
}

export interface Contact extends Base {
  fullname: string;
  email: string;
  title: string;
  message: string;
}
