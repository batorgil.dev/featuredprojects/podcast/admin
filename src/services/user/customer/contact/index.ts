import http from "services";
import { Contact, ContactFilterInput } from "./types";
import { PaginationResponse, SuccessResponse } from "types";

namespace contact {
  export const page = async (body: ContactFilterInput) =>
    http.post<PaginationResponse<Contact>>(`/user/customer/contact/page`, {
      body,
      hasAuth: true,
    });

  export const remove = async (id: string | number) =>
    http.del<SuccessResponse>(`/user/customer/contact/delete/${id}`, {
      hasAuth: true,
    });
}

export default contact;
