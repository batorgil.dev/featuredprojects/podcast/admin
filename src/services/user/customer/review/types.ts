import { AntdFile, Base, PaginationRequest } from "types";

export interface ReviewFilterInput extends PaginationRequest {
  fullname?: string;
  avatar_path?: string;
  comment?: string;
  created_at?: string[];
}

export interface ReviewCreateInput {
  fullname: string;
  avatar_path: string;
  star: number;
  comment: string;
  image?: AntdFile[];
}

export interface ReviewUpdateInput {
  fullname: string;
  avatar_path: string;
  star: number;
  comment: string;
  image?: AntdFile[];
}

export interface Review extends Base {
  fullname: string;
  avatar_path: string;
  star: number;
  comment: string;
}
