import http from "services";
import {
  Review,
  ReviewCreateInput,
  ReviewFilterInput,
  ReviewUpdateInput,
} from "./types";
import { PaginationResponse, SuccessResponse } from "types";

namespace review {
  export const page = async (body: ReviewFilterInput) =>
    http.post<PaginationResponse<Review>>(`/user/customer/review/page`, {
      body,
      hasAuth: true,
    });

  export const create = async (body: ReviewCreateInput) =>
    http.post<SuccessResponse>(`/user/customer/review/create`, {
      body,
      hasAuth: true,
    });

  export const update = async (id: string | number, body: ReviewUpdateInput) =>
    http.put<SuccessResponse>(`/user/customer/review/update/${id}`, {
      body,
      hasAuth: true,
    });

  export const remove = async (id: string | number) =>
    http.del<SuccessResponse>(`/user/customer/review/delete/${id}`, {
      hasAuth: true,
    });
}

export default review;
