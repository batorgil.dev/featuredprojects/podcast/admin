import { Base } from "types";

export interface LocaleRecordInput {
  name: string;
  lang: string;
}
export interface LocaleRecord extends Base {
  name: string;
  lang: string;
}

export const FirstTimeBool = {
  "0": {
    text: "Давтан",
  },
  "1": {
    text: "Анх удаа",
  },
};
