import ReactDOM from "react-dom/client";
import { ProAliasToken, ProConfigProvider } from "@ant-design/pro-provider";
import proEnUS from "@ant-design/pro-provider/lib/locale/en_US";
import { ConfigProvider as AntdConfigProvider } from "antd";
import enUS from "antd/lib/locale/en_US";
import { AppInitProvider } from "components";
import { BrowserRouter } from "react-router-dom";
import Routes from "routes";
import "./global.less";

const token = {
  colorPrimary: "#1AD993",
  fontFamily: "Inter, sans-serif",
  fontSize: 14,
  boxShadow: "0 0 0 rgba(3, 9, 21, 0.5)",
} as unknown as Partial<ProAliasToken>;

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <AntdConfigProvider
      locale={enUS}
      theme={{
        token: token,
      }}
    >
      <ProConfigProvider valueTypeMap={proEnUS as any} token={token}>
        <AppInitProvider>
          <BrowserRouter>
            <Routes />
          </BrowserRouter>
        </AppInitProvider>
      </ProConfigProvider>
    </AntdConfigProvider>
  </React.StrictMode>
);
