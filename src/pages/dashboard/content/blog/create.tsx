import { ArrowLeftOutlined } from "@ant-design/icons";
import { ProCard } from "@ant-design/pro-components";
import ProForm, {
  ProFormCheckbox,
  ProFormSelect,
  ProFormText,
  ProFormTextArea,
  ProFormUploadButton,
} from "@ant-design/pro-form";
import { useRequest } from "ahooks";
import { Button, Col, Divider, Form, Row, Space, message } from "antd";
import { FormBraft } from "components";
import { FC } from "react";
import { useNavigate } from "react-router-dom";
import blog from "services/user/content/blog";
import { BlogCreateInput } from "services/user/content/blog/types";
import file from "services/user/file";
import category from "services/user/settings/category";

const CreateInPage: FC = () => {
  const [form] = Form.useForm<BlogCreateInput>();
  const navigate = useNavigate();

  const create = useRequest(blog.create, {
    manual: true,
    onSuccess: () => {
      message.success({
        content: "Successfully saved",
      });
    },
    onError: (err) => {
      message.error({ content: err.message, style: { color: "red" } });
    },
  });

  const fetchCategories = useRequest(
    () =>
      category.page({
        is_all: true,
      }),
    {
      onError: (err) => {
        message.error(err.message);
      },
    }
  );

  const uploads = useRequest(file.uploads, {
    manual: true,
    onError: (err) => message.error(err.message),
  });

  return (
    <ProCard
      title={
        <Space>
          <Button
            type="dashed"
            onClick={() => {
              navigate(-1);
            }}
            icon={<ArrowLeftOutlined />}
          >
            Back
          </Button>
          <span>Create Your Blog</span>
        </Space>
      }
    >
      <ProForm<BlogCreateInput>
        title="Create"
        form={form}
        submitter={{
          searchConfig: {
            resetText: "Reset",
            submitText: "Save",
          },
        }}
        onFinish={async (values) => {
          if (values.image && values.image.length > 0) {
            const files = values.image;
            const uploadableFiles = file.getUploadableFiles(files);
            const uploadedFile = await uploads.runAsync({
              names: uploadableFiles.map((item) => item.name || "default"),
              files: uploadableFiles,
              bucket_name: "contents",
            });
            const uploadedFilePaths = uploadedFile.map((item) => item.path);
            const finalPaths = file.getPaths(
              "contents",
              uploadedFilePaths,
              files
            );
            values.cover_path = finalPaths[0];
          }
          values.image = undefined;

          await create.runAsync({
            ...values,
            description_html: values.description_html?.toHTML(),
            categories: values.category_ids?.map((id) => ({ id })) || [],
          });

          navigate("/dashboard/content/blog");
          return true;
        }}
      >
        <Divider style={{ margin: 0, padding: 0, marginBottom: 20 }} />
        <Row gutter={[12, 12]}>
          <Col sm={24} md={12} lg={8}>
            <ProFormText
              name="title"
              label="Title"
              placeholder="Title"
              rules={[{ required: true, message: "Title is required" }]}
            />
          </Col>
          <Col sm={24} md={12} lg={8}>
            <ProFormSelect
              style={{ width: "100%" }}
              label="Categories"
              name="category_ids"
              mode="multiple"
              fieldProps={{
                loading: fetchCategories.loading,
              }}
              options={fetchCategories.data?.items.map((item) => ({
                label: item.name,
                value: item.id,
              }))}
              placeholder="Select categories"
            />
          </Col>
          <Col sm={24} md={12} lg={8}>
            <ProFormCheckbox label="Featured" name="is_featured">
              Is Featured
            </ProFormCheckbox>
          </Col>
          <Col span={24}>
            <Row gutter={[12, 12]}>
              <Col sm={24} md={12} lg={12}>
                <ProFormTextArea
                  name="summary"
                  label="Summary"
                  placeholder="Summary"
                  rules={[{ required: true, message: "Summary is required" }]}
                />
              </Col>
              <Col sm={24} md={12} lg={12}>
                <ProFormUploadButton
                  required
                  label="Cover image"
                  name="image"
                  title="Image insert"
                  accept="image/*"
                  max={1}
                  fieldProps={{
                    listType: "picture-card",
                    beforeUpload: () => false,
                  }}
                />
              </Col>
            </Row>
          </Col>
          <Col span={24} style={{ marginBottom: 24 }}>
            <FormBraft name="description_html" />
          </Col>
        </Row>
      </ProForm>
    </ProCard>
  );
};

export default CreateInPage;
