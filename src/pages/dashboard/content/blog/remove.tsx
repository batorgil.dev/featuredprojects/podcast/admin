import { ModalForm, ProFormInstance, ProFormText } from "@ant-design/pro-form";
import { useRequest } from "ahooks";
import { message } from "antd";
import { deleteConfirm, deleteConfirmReg } from "config";
import { FC, useRef } from "react";
import blog from "services/user/content/blog";
import { Blog } from "services/user/content/blog/types";
import { DeleteConfirm } from "types";

interface Props {
  item?: Blog;
  onFinish: () => void;
  onClose: () => void;
}

const Remove: FC<Props> = ({ item, onFinish, onClose }) => {
  const formRef = useRef<ProFormInstance<DeleteConfirm>>();
  const remove = useRequest((item: Blog) => blog.remove(item.id), {
    manual: true,
    onSuccess: (_, params) =>
      message.success({
        content: `${params[0].title} removed`,
      }),
    onError: (err) => message.error(err.message),
  });
  return (
    <ModalForm<DeleteConfirm>
      width={500}
      open={!!item}
      formRef={formRef}
      requiredMark={false}
      title="Remove"
      modalProps={{
        bodyStyle: {
          paddingBottom: 0,
          paddingTop: "0.382rem",
        },
        onCancel: onClose,
      }}
      submitter={{
        searchConfig: {
          resetText: "Back",
          submitText: "Remove",
        },
        submitButtonProps: {
          danger: true,
        },
      }}
      onFinish={async () => {
        if (!!(await remove.runAsync(item!))) {
          onFinish();
          return true;
        }
        return false;
      }}
    >
      <ProFormText
        name="confirm"
        label={
          <div>
            <span>
              You selected "<strong>{item?.title}</strong>". Confirm your
              choose. This action cannot be undone. If you are sure, ente "
              <strong>{deleteConfirm}</strong>".
            </span>
          </div>
        }
        placeholder={deleteConfirm}
        rules={[
          {
            required: true,
            whitespace: false,
            pattern: deleteConfirmReg,
            message: `Enter "${deleteConfirm}" to confirm the deletion`,
          },
        ]}
      />
    </ModalForm>
  );
};

export default Remove;
