import {
  CheckCircleFilled,
  DeleteOutlined,
  EditOutlined,
  EyeOutlined,
  MoreOutlined,
  PlusOutlined,
} from "@ant-design/icons";
import {
  ProFormDateRangePicker,
  ProFormInstance,
  ProFormRadio,
  ProFormSelect,
  ProFormText,
} from "@ant-design/pro-form";
import { ActionType } from "@ant-design/pro-table";
import { useRequest } from "ahooks";
import { Button, Dropdown, Image, Space, Tag, message } from "antd";
import { DownloadButton, StatusTag } from "components";
import MainTable from "components/main_table";
import { FC, useRef, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import file from "services/user/file";
import category from "services/user/settings/category";
import { DefaultPagination, IsBoolEnum } from "types";
import { renderDate, tableCellFixed, tablePagination } from "utils";
import Remove from "./remove";
import podcast from "services/user/content/podcast";
import { Podcast } from "services/user/content/podcast/types";
import { ContentStatusEnum } from "services/user/content/blog/types";

const Podcasts: FC = () => {
  const navigate = useNavigate();

  const actionRef = useRef<ActionType>();
  const fetch = useRequest(podcast.page, {
    manual: true,
    onError: (err) => {
      message.error(err.message);
    },
  });

  const publish = useRequest(podcast.publish, {
    manual: true,
    onSuccess: () => {
      reload();
      message.success({
        content: "Published",
      });
    },
    onError: (err) => {
      message.error(err.message);
    },
  });

  const backToDraft = useRequest(podcast.backToDraft, {
    manual: true,
    onSuccess: () => {
      reload();
      message.success({
        content: "Moved back to draft",
      });
    },
    onError: (err) => {
      message.error(err.message);
    },
  });

  const fetchCategories = useRequest(
    () =>
      category.page({
        is_all: true,
      }),
    {
      onError: (err) => {
        message.error(err.message);
      },
    }
  );

  const [remove, setRemove] = useState<Podcast>();

  const ref = useRef<ProFormInstance>();

  const reload = () => {
    setRemove(undefined);
    actionRef.current?.reload();
  };

  return (
    <>
      <MainTable<Podcast>
        id="main-table"
        rowKey="id"
        scroll={{ x: "auto" }}
        actionRef={actionRef}
        search={false}
        pagination={DefaultPagination as any}
        headerTitle={
          <Space>
            <div
              style={{
                color: "#344054",
                fontWeight: 600,
                fontSize: 18,
                lineHeight: "28px",
                display: "flex",
                alignItems: "center",
                columnGap: "4px",
              }}
            >
              Podcast <Tag>{fetch.data?.total || 0}</Tag>
            </div>
            <ProFormRadio.Group
              noStyle
              key={"status"}
              name={"status"}
              style={{ width: "100%" }}
              radioType="button"
              initialValue={"all"}
              valueEnum={{ all: { text: "All" }, ...ContentStatusEnum }}
              placeholder="Search by categories"
            />
          </Space>
        }
        formRef={ref}
        tableAlertRender={false}
        columns={[
          {
            title: "№",
            width: 48,
            fixed: "left",
            dataIndex: "index",
            valueType: "index",
          },
          {
            ...tableCellFixed(200),
            ellipsis: true,
            sorter: true,
            title: "Title",
            valueType: "text",
            dataIndex: "title",
            filter: (
              <ProFormText
                noStyle
                className="w-full"
                name="title"
                placeholder="Search by Title"
              />
            ),
            render: (_, record) => (
              <Link to={`/dashboard/content/podcast/view/${record.id}`}>
                {record.title}
              </Link>
            ),
          },
          {
            ...tableCellFixed(200),
            ellipsis: true,
            sorter: true,
            title: "Summary",
            valueType: "text",
            dataIndex: "summary",
            filter: (
              <ProFormText
                noStyle
                className="w-full"
                name="summary"
                placeholder="Search by Summary"
              />
            ),
          },
          {
            ...tableCellFixed(80),
            ellipsis: true,
            sorter: true,
            search: false,
            title: "Cover image",
            valueType: "text",
            dataIndex: "cover_path",
            render: (_, record) => (
              <Image src={file.fileToUrl(record.cover_path)} width={80} />
            ),
          },
          {
            ...tableCellFixed(80),
            ellipsis: true,
            sorter: true,
            search: false,
            title: "Views",
            valueType: "text",
            dataIndex: "view_count",
          },
          {
            ...tableCellFixed(80),
            ellipsis: true,
            sorter: true,
            search: false,
            title: "Is Featured",
            valueType: "text",
            dataIndex: "is_featured",
            filter: (
              <ProFormSelect
                noStyle
                style={{ width: "100%" }}
                name="is_featured"
                valueEnum={IsBoolEnum}
                placeholder="Search by featured"
              />
            ),
            render: (_, record) => <StatusTag status={record.is_featured} />,
          },
          {
            ...tableCellFixed(80),
            ellipsis: true,
            hideInTable: true,
            title: "Category",
            valueType: "text",
            dataIndex: "category_ids",
            filter: (
              <ProFormSelect
                noStyle
                style={{ width: "100%" }}
                name="category_ids"
                mode="multiple"
                fieldProps={{
                  loading: fetchCategories.loading,
                }}
                options={fetchCategories.data?.items.map((item) => ({
                  label: item.name,
                  value: item.id,
                }))}
                placeholder="Search by categories"
              />
            ),
            render: (_, record) => <StatusTag status={record.is_featured} />,
          },
          {
            ...tableCellFixed(80),
            ellipsis: true,
            title: "Status",
            valueType: "select",
            valueEnum: ContentStatusEnum,
            dataIndex: "status",
          },
          {
            ...tableCellFixed(100),
            ellipsis: true,
            title: "Published at",
            dataIndex: "published_at",
            defaultSortOrder: "descend",
            valueType: "dateRange",
            sorter: true,
            filter: (
              <ProFormDateRangePicker
                noStyle
                className="w-full"
                name="published_at"
                placeholder={["Published at", "Published at"]}
              />
            ),
            render: (_, record) => renderDate(record.published_at, true),
          },
          {
            ...tableCellFixed(68),
            fixed: "right",
            ellipsis: true,
            title: "Action",
            search: false,
            dataIndex: "action",
            render: (_, record) => {
              const items: any[] = [
                {
                  key: "view",
                  label: "View",
                  icon: <EyeOutlined />,
                  onClick: () => {
                    navigate(`/dashboard/content/podcast/view/${record.id}`);
                  },
                },
                {
                  key: "update",
                  label: "Edit",
                  icon: <EditOutlined />,
                  onClick: () => {
                    navigate(`/dashboard/content/podcast/update/${record.id}`);
                  },
                },
                {
                  key: "remove",
                  label: "Remove",
                  danger: true,
                  icon: <DeleteOutlined />,
                  onClick: () => {
                    setRemove(record);
                  },
                },
              ];

              if (record.status === "draft") {
                items.push({
                  key: "publish",
                  label: "Publish",
                  type: "primary",
                  icon: <CheckCircleFilled style={{ color: "#1ad993" }} />,
                  loading: publish.loading,
                  onClick: () => {
                    publish.run(record.id);
                  },
                });
              }
              if (record.status === "published") {
                items.push({
                  key: "backToDraft",
                  label: "Move back to Draft",
                  type: "primary",
                  icon: <CheckCircleFilled style={{ color: "#1ad993" }} />,
                  loading: backToDraft.loading,
                  onClick: () => {
                    backToDraft.run(record.id);
                  },
                });
              }
              return (
                <Dropdown
                  menu={{
                    items: items,
                  }}
                  placement="bottomRight"
                >
                  <Button style={{ borderColor: "white" }}>
                    <MoreOutlined />
                  </Button>
                </Dropdown>
              );
            },
          },
        ]}
        request={async (params, sort) => {
          if ((params as any).status === "all") {
            delete (params as any).status;
          }
          const result = await fetch.runAsync({
            ...tablePagination({ ...params }, sort),
            is_all: true,
          });
          return {
            data: result?.items || [],
            total: result?.total || 0,
            success: true,
          };
        }}
        toolbar={{
          multipleLine: true,
          actions: [
            <DownloadButton
              key="download"
              filename="Podcast"
              formRef={ref.current}
            />,
            <Link to="/dashboard/content/podcast/create">
              <Button type="primary" icon={<PlusOutlined />}>
                Create Podcast
              </Button>
            </Link>,
          ],
        }}
      />
      <Remove
        item={remove}
        onFinish={() => {
          reload();
        }}
        onClose={() => {
          setRemove(undefined);
        }}
      />
    </>
  );
};

export default Podcasts;
