import { ArrowLeftOutlined } from "@ant-design/icons";
import { ProCard } from "@ant-design/pro-components";
import { useRequest } from "ahooks";
import {
  Avatar,
  Button,
  Col,
  Divider,
  Image,
  Row,
  Skeleton,
  Space,
  Tag,
  Typography,
} from "antd";
import NotFound from "pages/exceptions/NotFound";
import { FC, useEffect } from "react";
import { Link, useNavigate, useParams } from "react-router-dom";
import podcast from "services/user/content/podcast";
import { Podcast } from "services/user/content/podcast/types";
import file from "services/user/file";

const ViewInPage: FC = () => {
  const { id: podcastId } = useParams();

  const navigate = useNavigate();
  const fetch = useRequest(podcast.get, { manual: true });

  const { run } = fetch;
  useEffect(() => {
    if (podcastId && Number.parseInt(podcastId || "")) {
      run(podcastId);
    }
  }, [run, podcastId]);

  if (!Number.parseInt(podcastId || "")) {
    return <NotFound />;
  }

  if (fetch.loading) {
    return (
      <ProCard
        title={
          <Space>
            <Button
              type="dashed"
              onClick={() => {
                navigate(-1);
              }}
              icon={<ArrowLeftOutlined />}
            >
              Back
            </Button>
            <span>View loading ...</span>
          </Space>
        }
      >
        <Skeleton loading />
      </ProCard>
    );
  }

  if (fetch.data) {
    return <ViewWithData data={fetch.data} />;
  }
  return <NotFound />;
};

const ViewWithData: FC<{ data: Podcast }> = ({ data }) => {
  const navigate = useNavigate();

  return (
    <ProCard
      title={
        <Space>
          <Button
            type="dashed"
            onClick={() => {
              navigate(-1);
            }}
            icon={<ArrowLeftOutlined />}
          >
            Back
          </Button>
          <span>{data.title}</span>
          {data.is_featured && <Tag color="green">Featured</Tag>}
        </Space>
      }
    >
      <Divider style={{ margin: 0, padding: 0, marginBottom: 20 }} />
      <Row gutter={[12, 12]}>
        <Col span={24}>
          <Typography.Paragraph style={{ margin: 0, padding: 0 }}>
            <strong>Summary: </strong>
            {data.summary}
          </Typography.Paragraph>
        </Col>
        <Col span={24}>
          <Space>
            <Typography.Paragraph style={{ margin: 0, padding: 0 }}>
              <strong>Categories: </strong>
            </Typography.Paragraph>
            {data.categories?.map((ctg) => (
              <Tag key={ctg.id}>{ctg.name}</Tag>
            ))}
          </Space>
        </Col>
        <Col span={24}>
          <Space>
            <Typography.Paragraph style={{ margin: 0, padding: 0 }}>
              <strong>Podcast links: </strong>
            </Typography.Paragraph>
            {data.listen_ons?.map((listenOn) => (
              <Link to={listenOn.url} target="_blank">
                <Tag key={listenOn.id}>
                  {listenOn.podcast_provider?.name || "-"}
                </Tag>
              </Link>
            ))}
          </Space>
        </Col>
        <Col span={24}>
          <Space>
            <Typography.Paragraph style={{ margin: 0, padding: 0 }}>
              <strong>Audio: </strong>
            </Typography.Paragraph>
            <audio src={file.fileToUrl(data.audio_path)} controls />
          </Space>
        </Col>
        <Col span={24} style={{ display: "flex", justifyContent: "center" }}>
          <Image src={file.fileToUrl(data.cover_path)} height={400} />
        </Col>
        <Col
          span={24}
          dangerouslySetInnerHTML={{ __html: data.description_html }}
        ></Col>
        <Col span={24} style={{ maxWidth: 625 }}>
          <div
            style={{
              borderLeft: "16px solid #503AE7",
              display: "flex",
              padding: 12,
              alignItems: "center",
            }}
          >
            <div style={{ flex: 1, display: "block" }}>
              <div>
                <Avatar
                  src={file.fileToUrl(data.author?.avatar_path || "")}
                  alt="AV"
                />
              </div>
              <div style={{ fontSize: 16, fontWeight: "bold" }}>
                {data.author?.first_name} {data.author?.last_name}
              </div>
              <div>{data.author?.position}</div>
            </div>
            <div style={{ flex: 2 }}>
              <div>{data.author?.bio}</div>
            </div>
          </div>
        </Col>
      </Row>
    </ProCard>
  );
};

export default ViewInPage;
