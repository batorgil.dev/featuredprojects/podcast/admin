import { ArrowLeftOutlined } from "@ant-design/icons";
import { ProCard } from "@ant-design/pro-components";
import ProForm, {
  ProFormCheckbox,
  ProFormList,
  ProFormSelect,
  ProFormText,
  ProFormTextArea,
  ProFormUploadButton,
} from "@ant-design/pro-form";
import { useRequest } from "ahooks";
import {
  Button,
  Col,
  Divider,
  Form,
  Row,
  Skeleton,
  Space,
  message,
} from "antd";
import { BraftEditorPropStated } from "components";
import NotFound from "pages/exceptions/NotFound";
import { FC, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import podcast from "services/user/content/podcast";
import {
  Podcast,
  PodcastUpdateInput,
} from "services/user/content/podcast/types";
import file from "services/user/file";
import category from "services/user/settings/category";
import podcastProvider from "services/user/settings/podcast_provider";

const UpdateInPage: FC = () => {
  const { id: podcastId } = useParams();

  const navigate = useNavigate();
  const fetch = useRequest(podcast.get, { manual: true });

  const { run } = fetch;
  useEffect(() => {
    if (podcastId && Number.parseInt(podcastId || "")) {
      run(podcastId);
    }
  }, [run, podcastId]);

  if (!Number.parseInt(podcastId || "")) {
    return <NotFound />;
  }

  if (fetch.loading) {
    return (
      <ProCard
        title={
          <Space>
            <Button
              type="dashed"
              onClick={() => {
                navigate(-1);
              }}
              icon={<ArrowLeftOutlined />}
            >
              Back
            </Button>
            <span>Update Your Podcast</span>
          </Space>
        }
      >
        <Skeleton loading />
      </ProCard>
    );
  }

  if (fetch.data) {
    return <UpdateWithData data={fetch.data} />;
  }
  return <NotFound />;
};

const UpdateWithData: FC<{ data: Podcast }> = ({ data }) => {
  const [descriptionHtml, setDescriptionHtml] = useState<any>();
  const [form] = Form.useForm<PodcastUpdateInput>();
  const navigate = useNavigate();

  const updateBlog = useRequest(podcast.update, {
    manual: true,
    onSuccess: () => {
      message.success({
        content: "Successfully saved",
      });
    },
    onError: (err) => {
      message.error({ content: err.message, style: { color: "red" } });
    },
  });

  const fetchCategories = useRequest(
    () =>
      category.page({
        is_all: true,
      }),
    {
      onError: (err) => {
        message.error(err.message);
      },
    }
  );

  const fetchPodcastProviders = useRequest(
    () =>
      podcastProvider.page({
        is_all: true,
      }),
    {
      onSuccess: (res) => {
        form.setFieldValue(
          "listen_ons",
          res?.items.map((i) => ({
            podcast_provider_id: i.id,
            url:
              data.listen_ons?.find((item) => item.podcast_provider_id === i.id)
                ?.url || "",
          }))
        );
      },
      onError: (err) => {
        message.error(err.message);
      },
    }
  );

  const uploads = useRequest(file.uploads, {
    manual: true,
    onError: (err) => message.error(err.message),
  });

  const { setFieldsValue } = form;
  useEffect(() => {
    setFieldsValue({
      cover_path: data.cover_path,
      audio_path: data.audio_path,
      category_ids: data.categories?.map((item) => item.id),
      is_featured: data.is_featured,
      summary: data.summary,
      title: data.title,
      audio: data.audio_path
        ? [
            {
              uid: data.audio_path,
              name: file.getFileName(data.audio_path),
              status: "done",
              response: "",
              url: file.fileToUrl(data.audio_path || ""),
            },
          ]
        : [],
      image: data.cover_path
        ? [
            {
              uid: data.cover_path,
              name: file.getFileName(data.cover_path),
              status: "done",
              response: "",
              url: file.fileToUrl(data.cover_path || ""),
            },
          ]
        : [],
    });
  }, [data, setFieldsValue]);

  return (
    <ProCard
      title={
        <Space>
          <Button
            type="dashed"
            onClick={() => {
              navigate(-1);
            }}
            icon={<ArrowLeftOutlined />}
          >
            Back
          </Button>
          <span>Update {data.title}</span>
        </Space>
      }
    >
      <ProForm<PodcastUpdateInput>
        title="Create"
        form={form}
        submitter={{
          searchConfig: {
            resetText: "Reset",
            submitText: "Save",
          },
        }}
        onFinish={async (values) => {
          if (values.image && values.image.length > 0) {
            const files = values.image;
            const uploadableFiles = file.getUploadableFiles(files);
            const uploadedFile = await uploads.runAsync({
              names: uploadableFiles.map((item) => item.name || "default"),
              files: uploadableFiles,
              bucket_name: "contents",
            });
            const uploadedFilePaths = uploadedFile.map((item) => item.path);
            const finalPaths = file.getPaths(
              "contents",
              uploadedFilePaths,
              files
            );
            values.cover_path = finalPaths[0];
          }
          values.image = undefined;

          if (values.audio && values.audio.length > 0) {
            const files = values.audio;
            const uploadableFiles = file.getUploadableFiles(files);
            const uploadedFile = await uploads.runAsync({
              names: uploadableFiles.map((item) => item.name || "default"),
              files: uploadableFiles,
              bucket_name: "contents",
            });
            const uploadedFilePaths = uploadedFile.map((item) => item.path);
            const finalPaths = file.getPaths(
              "contents",
              uploadedFilePaths,
              files
            );
            values.audio_path = finalPaths[0];
          }
          values.audio = undefined;

          await updateBlog.runAsync(data.id, {
            ...values,
            description_html: descriptionHtml?.toHTML(),
            categories: values.category_ids?.map((id) => ({ id })) || [],
          });

          navigate("/dashboard/content/podcast");
          return true;
        }}
      >
        <Divider style={{ margin: 0, padding: 0, marginBottom: 20 }} />
        <Row gutter={[12, 12]}>
          <Col sm={24} md={12} lg={8}>
            <ProFormText
              name="title"
              label="Title"
              placeholder="Title"
              rules={[{ required: true, message: "Title is required" }]}
            />
          </Col>
          <Col sm={24} md={12} lg={8}>
            <ProFormSelect
              style={{ width: "100%" }}
              label="Categories"
              name="category_ids"
              mode="multiple"
              fieldProps={{
                loading: fetchCategories.loading,
              }}
              options={fetchCategories.data?.items.map((item) => ({
                label: item.name,
                value: item.id,
              }))}
              placeholder="Select categories"
            />
          </Col>
          <Col sm={24} md={12} lg={8}>
            <ProFormCheckbox label="Featured" name="is_featured">
              Is Featured
            </ProFormCheckbox>
          </Col>
          <Col span={24}>
            <Row gutter={[12, 12]}>
              <Col sm={24} md={12} lg={8}>
                <ProFormTextArea
                  name="summary"
                  label="Summary"
                  placeholder="Summary"
                  rules={[{ required: true, message: "Summary is required" }]}
                />
              </Col>
              <Col sm={24} md={12} lg={8}>
                <ProFormUploadButton
                  required
                  label="Cover image"
                  name="image"
                  title="Image insert"
                  accept="image/*"
                  max={1}
                  fieldProps={{
                    listType: "picture-card",
                    beforeUpload: () => false,
                  }}
                />
              </Col>
              <Col sm={24} md={12} lg={8}>
                <ProFormUploadButton
                  required
                  name="audio"
                  title="Audio insert"
                  accept="audio/*"
                  max={1}
                  fieldProps={{
                    beforeUpload: () => false,
                  }}
                />
              </Col>
            </Row>
          </Col>
          <Col span={24}>
            {fetchPodcastProviders.data?.items &&
              fetchPodcastProviders.data?.items?.length > 0 && (
                <ProFormList
                  label="Podcast links"
                  name="listen_ons"
                  className="pro-form-list-w-full"
                  creatorButtonProps={false}
                  deleteIconProps={false}
                  copyIconProps={false}
                >
                  {(_, index) => {
                    const socialProviderId = form.getFieldValue([
                      "listen_ons",
                      index,
                      "podcast_provider_id",
                    ]);
                    const socialProvider =
                      fetchPodcastProviders.data?.items?.find(
                        (item) => item.id === socialProviderId
                      );
                    return (
                      <div style={{ width: "100%" }}>
                        <ProFormText
                          style={{ width: "100%" }}
                          name="url"
                          rules={[{ type: "url", message: "must be url" }]}
                          fieldProps={{ addonBefore: socialProvider?.name }}
                        />
                      </div>
                    );
                  }}
                </ProFormList>
              )}
          </Col>
          <Col span={24} style={{ marginBottom: 24 }}>
            <BraftEditorPropStated
              initialValue={data.description_html}
              data={descriptionHtml}
              setData={setDescriptionHtml}
            />
          </Col>
        </Row>
      </ProForm>
    </ProCard>
  );
};

export default UpdateInPage;
