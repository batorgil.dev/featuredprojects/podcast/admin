import { PlusCircleOutlined } from "@ant-design/icons";
import { ModalForm, ProFormText } from "@ant-design/pro-form";
import { useRequest } from "ahooks";
import { Button, Divider, Form, message } from "antd";
import useDrawerWidth from "hooks/useDrawerWidth";
import { FC } from "react";
import category from "services/user/settings/category";
import { CategoryCreateInput } from "services/user/settings/category/types";

interface Props {
  onFinish: () => void;
}

const Create: FC<Props> = ({ onFinish }) => {
  const [form] = Form.useForm<CategoryCreateInput>();
  const create = useRequest(category.create, {
    manual: true,
    onSuccess: () => {
      message.success({
        content: "Successfully saved",
      });
    },
    onError: (err) => {
      message.error({ content: err.message, style: { color: "red" } });
    },
  });

  const drawerWidth = useDrawerWidth(620);

  return (
    <ModalForm<CategoryCreateInput>
      title="Create"
      width={drawerWidth}
      form={form}
      trigger={
        <Button
          type="primary"
          key="primary"
          size="middle"
          style={{ boxShadow: "10px" }}
          icon={<PlusCircleOutlined />}
          onClick={() => {
            form.resetFields();
          }}
        >
          Create
        </Button>
      }
      modalProps={{
        forceRender: true,
        destroyOnClose: true,
      }}
      submitter={{
        searchConfig: {
          resetText: "Back",
          submitText: "Save",
        },
      }}
      onFinish={async (values) => {
        await create.runAsync({
          ...values,
        });
        onFinish();
        return true;
      }}
    >
      <Divider />
      <ProFormText
        name="name"
        label="Name"
        placeholder="Name is required"
        rules={[{ required: true, message: "Name is required" }]}
      />
    </ModalForm>
  );
};

export default Create;
