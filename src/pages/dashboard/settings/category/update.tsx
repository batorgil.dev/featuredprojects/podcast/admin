import { ModalForm, ProFormText } from "@ant-design/pro-form";
import { useRequest } from "ahooks";
import { Divider, Form, message } from "antd";
import useDrawerWidth from "hooks/useDrawerWidth";
import { FC, useEffect } from "react";
import category from "services/user/settings/category";
import {
  Category,
  CategoryUpdateInput,
} from "services/user/settings/category/types";

interface Props {
  data?: Category;
  onClose: () => void;
  onFinish: () => void;
}

const Update: FC<Props> = ({ data, onFinish, onClose }) => {
  const [form] = Form.useForm<CategoryUpdateInput>();
  const update = useRequest(category.update, {
    manual: true,
    onSuccess: () => {
      message.success({
        content: "Successfully saved",
      });
    },
    onError: (err) => {
      message.error({ content: err.message, style: { color: "red" } });
    },
  });

  const drawerWidth = useDrawerWidth(620);
  const setFieldsValue = form.setFieldsValue;

  useEffect(() => {
    if (data) {
      setFieldsValue({
        name: data.name,
      });
    }
  }, [data, setFieldsValue]);

  return (
    <ModalForm<CategoryUpdateInput>
      title="Edit"
      width={drawerWidth}
      form={form}
      open={!!data}
      onOpenChange={(isOpen) => {
        if (!isOpen) onClose();
      }}
      modalProps={{
        forceRender: true,
        destroyOnClose: true,
      }}
      submitter={{
        searchConfig: {
          resetText: "Back",
          submitText: "Save",
        },
      }}
      onFinish={async (values) => {
        await update.runAsync(data?.id || 0, {
          ...values,
        });
        onFinish();
        return true;
      }}
    >
      <Divider />
      <ProFormText
        name="name"
        label="Name"
        placeholder="Name is required"
        rules={[{ required: true, message: "Name is required" }]}
      />
    </ModalForm>
  );
};

export default Update;
