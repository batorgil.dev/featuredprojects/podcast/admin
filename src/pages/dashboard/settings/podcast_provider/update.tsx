import {
  ModalForm,
  ProFormText,
  ProFormUploadButton,
} from "@ant-design/pro-form";
import { useRequest } from "ahooks";
import { Divider, Form, message } from "antd";
import useDrawerWidth from "hooks/useDrawerWidth";
import { FC, useEffect } from "react";
import file from "services/user/file";
import podcastProvider from "services/user/settings/podcast_provider";
import {
  PodcastProvider,
  PodcastProviderUpdateInput,
} from "services/user/settings/podcast_provider/types";

interface Props {
  data?: PodcastProvider;
  onClose: () => void;
  onFinish: () => void;
}

const Update: FC<Props> = ({ data, onFinish, onClose }) => {
  const [form] = Form.useForm<PodcastProviderUpdateInput>();
  const update = useRequest(podcastProvider.update, {
    manual: true,
    onSuccess: () => {
      message.success({
        content: "Successfully saved",
      });
    },
    onError: (err) => {
      message.error({ content: err.message, style: { color: "red" } });
    },
  });

  const uploads = useRequest(file.uploads, {
    manual: true,
    onError: (err) => message.error(err.message),
  });

  const drawerWidth = useDrawerWidth(620);
  const setFieldsValue = form.setFieldsValue;

  useEffect(() => {
    if (data) {
      setFieldsValue({
        logo_path: data.logo_path,
        name: data.name,
        url: data.url,
        image: data.logo_path
          ? [
              {
                uid: data.logo_path,
                name: file.getFileName(data.logo_path),
                status: "done",
                response: "",
                url: file.fileToUrl(data.logo_path || ""),
              },
            ]
          : [],
      });
    }
  }, [data, setFieldsValue]);

  return (
    <ModalForm<PodcastProviderUpdateInput>
      title="Edit"
      width={drawerWidth}
      form={form}
      open={!!data}
      onOpenChange={(isOpen) => {
        if (!isOpen) onClose();
      }}
      modalProps={{
        forceRender: true,
        destroyOnClose: true,
      }}
      submitter={{
        searchConfig: {
          resetText: "Back",
          submitText: "Save",
        },
      }}
      onFinish={async (values) => {
        if (values.image && values.image.length > 0) {
          const files = values.image;
          const uploadableFiles = file.getUploadableFiles(files);
          const uploadedFile = await uploads.runAsync({
            names: uploadableFiles.map((item) => item.name || "default"),
            files: uploadableFiles,
            bucket_name: "sponsers",
          });
          const uploadedFilePaths = uploadedFile.map((item) => item.path);
          const finalPaths = file.getPaths(
            "sponsers",
            uploadedFilePaths,
            files
          );
          values.logo_path = finalPaths[0];
        }
        values.image = undefined;

        await update.runAsync(data?.id || 0, {
          ...values,
        });
        onFinish();
        return true;
      }}
    >
      <Divider />
      <ProFormUploadButton
        required
        label="Logo"
        name="image"
        title="Image insert"
        accept="image/*"
        max={1}
        fieldProps={{
          listType: "picture-card",
          beforeUpload: () => false,
        }}
      />
      <ProFormText
        name="name"
        label="Name"
        placeholder="Name"
        rules={[{ required: true, message: "Name is required" }]}
      />
      <ProFormText
        name="url"
        label="Url"
        placeholder="Url"
        rules={[{ type: "url", message: "Url is required" }]}
      />
    </ModalForm>
  );
};

export default Update;
