import { DeleteOutlined, EditOutlined, MoreOutlined } from "@ant-design/icons";
import {
  ProFormDateRangePicker,
  ProFormInstance,
  ProFormText,
} from "@ant-design/pro-form";
import { ActionType } from "@ant-design/pro-table";
import { useRequest } from "ahooks";
import { Button, Dropdown, Image, Tag, message } from "antd";
import MainTable from "components/main_table";
import { FC, useRef, useState } from "react";
import { renderDate, tableCellFixed, tablePagination } from "utils";
import Create from "./create";
import Remove from "./remove";
import Update from "./update";
import { DownloadButton } from "components";
import file from "services/user/file";
import podcastProvider from "services/user/settings/podcast_provider";
import { PodcastProvider } from "services/user/settings/podcast_provider/types";

const PodcastProviders: FC = () => {
  const actionRef = useRef<ActionType>();
  const fetch = useRequest(podcastProvider.page, {
    manual: true,
    onError: (err) => {
      message.error(err.message);
    },
  });

  const [update, setUpdate] = useState<PodcastProvider>();
  const [remove, setRemove] = useState<PodcastProvider>();

  const ref = useRef<ProFormInstance>();

  const reload = () => {
    setRemove(undefined);
    setUpdate(undefined);
    actionRef.current?.reload();
  };

  return (
    <>
      <MainTable<PodcastProvider>
        id="main-table"
        rowKey="id"
        scroll={{ x: "auto" }}
        actionRef={actionRef}
        search={false}
        pagination={false}
        headerTitle={
          <div
            style={{
              color: "#344054",
              fontWeight: 600,
              fontSize: 18,
              lineHeight: "28px",
              display: "flex",
              alignItems: "center",
              columnGap: "4px",
            }}
          >
            Podcast accounts <Tag>{fetch.data?.total || 0}</Tag>
          </div>
        }
        formRef={ref}
        tableAlertRender={false}
        columns={[
          {
            title: "№",
            width: 48,
            fixed: "left",
            dataIndex: "index",
            valueType: "index",
          },
          {
            ...tableCellFixed(80),
            ellipsis: true,
            sorter: true,
            search: false,
            title: "Logo",
            valueType: "text",
            dataIndex: "logo_path",
            render: (_, record) => (
              <Image src={file.fileToUrl(record.logo_path)} width={80} />
            ),
          },
          {
            ...tableCellFixed(200),
            ellipsis: true,
            sorter: true,
            title: "Name",
            valueType: "text",
            dataIndex: "name",
            filter: (
              <ProFormText
                noStyle
                className="w-full"
                name="name"
                placeholder="Search by Name"
              />
            ),
          },
          {
            ...tableCellFixed(200),
            ellipsis: true,
            sorter: true,
            title: "URL",
            valueType: "text",
            dataIndex: "url",
            filter: (
              <ProFormText
                noStyle
                className="w-full"
                name="url"
                placeholder="Search by URL"
              />
            ),
          },
          {
            ...tableCellFixed(100),
            ellipsis: true,
            title: "Created at",
            dataIndex: "created_at",
            defaultSortOrder: "descend",
            valueType: "dateRange",
            sorter: true,
            filter: (
              <ProFormDateRangePicker
                noStyle
                className="w-full"
                name="created_at"
                placeholder={["Start", "End"]}
              />
            ),
            render: (_, record) => renderDate(record.created_at, true),
          },
          {
            ...tableCellFixed(68),
            fixed: "right",
            ellipsis: true,
            title: "Action",
            search: false,
            dataIndex: "action",
            render: (_, record) => (
              <Dropdown
                menu={{
                  items: [
                    {
                      key: "update",
                      label: "Edit",
                      icon: <EditOutlined />,
                      onClick: () => {
                        setUpdate(record);
                      },
                    },
                    {
                      key: "remove",
                      label: "Remove",
                      danger: true,
                      icon: <DeleteOutlined />,
                      onClick: () => {
                        setRemove(record);
                      },
                    },
                  ],
                }}
                placement="bottomRight"
              >
                <Button style={{ borderColor: "white" }}>
                  <MoreOutlined />
                </Button>
              </Dropdown>
            ),
          },
        ]}
        request={async (params, sort) => {
          const result = await fetch.runAsync({
            ...tablePagination({ ...params }, sort),
            is_all: true,
          });
          return {
            data: result?.items || [],
            total: result?.total || 0,
            success: true,
          };
        }}
        toolbar={{
          multipleLine: true,
          actions: [
            <DownloadButton
              filename="Podcast accounts"
              formRef={ref.current}
            />,
            <Create key="create" onFinish={() => reload()} />,
          ],
        }}
      />
      <Update
        data={update}
        onFinish={() => {
          reload();
        }}
        onClose={() => {
          setUpdate(undefined);
        }}
      />
      <Remove
        item={remove}
        onFinish={() => {
          reload();
        }}
        onClose={() => {
          setRemove(undefined);
        }}
      />
    </>
  );
};

export default PodcastProviders;
