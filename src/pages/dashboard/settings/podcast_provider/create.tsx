import { PlusCircleOutlined } from "@ant-design/icons";
import {
  ModalForm,
  ProFormText,
  ProFormUploadButton,
} from "@ant-design/pro-form";
import { useRequest } from "ahooks";
import { Button, Divider, Form, message } from "antd";
import useDrawerWidth from "hooks/useDrawerWidth";
import { FC } from "react";
import file from "services/user/file";
import podcastProvider from "services/user/settings/podcast_provider";
import { PodcastProviderCreateInput } from "services/user/settings/podcast_provider/types";

interface Props {
  onFinish: () => void;
}

const Create: FC<Props> = ({ onFinish }) => {
  const [form] = Form.useForm<PodcastProviderCreateInput>();
  const create = useRequest(podcastProvider.create, {
    manual: true,
    onSuccess: () => {
      message.success({
        content: "Successfully saved",
      });
    },
    onError: (err) => {
      message.error({ content: err.message, style: { color: "red" } });
    },
  });

  const uploads = useRequest(file.uploads, {
    manual: true,
    onError: (err) => message.error(err.message),
  });

  const drawerWidth = useDrawerWidth(620);

  return (
    <ModalForm<PodcastProviderCreateInput>
      title="Create"
      width={drawerWidth}
      form={form}
      trigger={
        <Button
          type="primary"
          key="primary"
          size="middle"
          style={{ boxShadow: "10px" }}
          icon={<PlusCircleOutlined />}
          onClick={() => {
            form.resetFields();
          }}
        >
          Create
        </Button>
      }
      modalProps={{
        forceRender: true,
        destroyOnClose: true,
      }}
      submitter={{
        searchConfig: {
          resetText: "Back",
          submitText: "Save",
        },
      }}
      onFinish={async (values) => {
        if (values.image && values.image.length > 0) {
          const files = values.image;
          const uploadableFiles = file.getUploadableFiles(files);
          const uploadedFile = await uploads.runAsync({
            names: uploadableFiles.map((item) => item.name || "default"),
            files: uploadableFiles,
            bucket_name: "sponsers",
          });
          const uploadedFilePaths = uploadedFile.map((item) => item.path);
          const finalPaths = file.getPaths(
            "sponsers",
            uploadedFilePaths,
            files
          );
          values.logo_path = finalPaths[0];
        }
        values.image = undefined;

        await create.runAsync({
          ...values,
        });
        onFinish();
        return true;
      }}
    >
      <Divider />
      <ProFormUploadButton
        required
        label="Logo"
        name="image"
        title="Image insert"
        accept="image/*"
        max={1}
        fieldProps={{
          listType: "picture-card",
          beforeUpload: () => false,
        }}
      />
      <ProFormText
        name="name"
        label="Name"
        placeholder="Name"
        rules={[{ required: true, message: "Name is required" }]}
      />
      <ProFormText
        name="url"
        label="Url"
        placeholder="Url"
        rules={[{ type: "url", message: "Url is required" }]}
      />
    </ModalForm>
  );
};

export default Create;
