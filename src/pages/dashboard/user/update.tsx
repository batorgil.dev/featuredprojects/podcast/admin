import {
  ModalForm,
  ProFormCheckbox,
  ProFormList,
  ProFormSelect,
  ProFormText,
  ProFormTextArea,
  ProFormUploadButton,
} from "@ant-design/pro-form";
import { useRequest } from "ahooks";
import { Col, Divider, Form, Row, message } from "antd";
import useDrawerWidth from "hooks/useDrawerWidth";
import { FC, useEffect } from "react";
import file from "services/user/file";
import { SocialProvider } from "services/user/settings/social_provider/types";
import user from "services/user/user";
import { User, UserRoleEnum, UserUpdateInput } from "services/user/user/types";

interface Props {
  data?: User;
  socialProviders?: SocialProvider[];
  onClose: () => void;
  onFinish: () => void;
}

const Update: FC<Props> = ({
  data,
  socialProviders = [],
  onFinish,
  onClose,
}) => {
  const [form] = Form.useForm<UserUpdateInput>();
  const update = useRequest(user.update, {
    manual: true,
    onSuccess: () => {
      message.success({
        content: "Successfully saved",
      });
    },
    onError: (err) => {
      message.error({ content: err.message, style: { color: "red" } });
    },
  });

  const uploads = useRequest(file.uploads, {
    manual: true,
    onError: (err) => message.error(err.message),
  });

  const drawerWidth = useDrawerWidth(620);
  const setFieldsValue = form.setFieldsValue;

  useEffect(() => {
    if (data) {
      setFieldsValue({
        avatar_path: data.avatar_path,
        last_name: data.last_name,
        first_name: data.first_name,
        bio: data.bio,
        email: data.email,
        is_active: data.is_active,
        position: data.position,
        role: data.role,
        social_links: socialProviders.map((i) => ({
          social_provider_id: i.id,
          url:
            data.social_links?.find((item) => item.social_provider_id === i.id)
              ?.url || "",
        })),
        image: data.avatar_path
          ? [
              {
                uid: data.avatar_path,
                name: file.getFileName(data.avatar_path),
                status: "done",
                response: "",
                url: file.fileToUrl(data.avatar_path || ""),
              },
            ]
          : [],
      });
    }
  }, [data, socialProviders, setFieldsValue]);

  return (
    <ModalForm<UserUpdateInput>
      title="Edit"
      width={drawerWidth}
      form={form}
      open={!!data}
      onOpenChange={(isOpen) => {
        if (!isOpen) onClose();
      }}
      modalProps={{
        forceRender: true,
        destroyOnClose: true,
      }}
      submitter={{
        searchConfig: {
          resetText: "Back",
          submitText: "Save",
        },
      }}
      onFinish={async (values) => {
        if (values.image && values.image.length > 0) {
          const files = values.image;
          const uploadableFiles = file.getUploadableFiles(files);
          const uploadedFile = await uploads.runAsync({
            names: uploadableFiles.map((item) => item.name || "default"),
            files: uploadableFiles,
            bucket_name: "avatars",
          });
          const uploadedFilePaths = uploadedFile.map((item) => item.path);
          const finalPaths = file.getPaths("avatars", uploadedFilePaths, files);
          values.avatar_path = finalPaths[0];
        }
        values.image = undefined;

        await update.runAsync(data?.id || 0, {
          ...values,
        });
        onFinish();
        return true;
      }}
    >
      <Divider />
      <ProFormUploadButton
        required
        label="Avatar"
        name="image"
        title="Image insert"
        accept="image/*"
        max={1}
        fieldProps={{
          listType: "picture-card",
          beforeUpload: () => false,
        }}
      />
      <Row gutter={[12, 12]}>
        <Col sm={24} md={12}>
          <ProFormText
            name="first_name"
            label="First name"
            placeholder="First name"
            rules={[{ required: true, message: "First name is required" }]}
          />
        </Col>
        <Col sm={24} md={12}>
          <ProFormText
            name="last_name"
            label="Last name"
            placeholder="Last name"
            rules={[{ required: true, message: "Last name is required" }]}
          />
        </Col>
        <Col sm={24} md={12}>
          <ProFormText
            name="email"
            label="Email"
            placeholder="Email"
            rules={[
              { required: true, message: "Email is required" },
              { type: "email", message: "Value format is not email" },
            ]}
          />
        </Col>
        <Col sm={24} md={12}>
          <ProFormText
            name="position"
            label="Position"
            placeholder="Position"
            rules={[{ required: true, message: "Position is required" }]}
          />
        </Col>
        <Col sm={24} md={24}>
          <ProFormTextArea
            name="bio"
            label="Bio"
            placeholder="Bio"
            rules={[{ required: true, message: "Bio is required" }]}
          />
        </Col>
        <Col sm={24} md={12}>
          <ProFormSelect
            name="role"
            label="Role"
            placeholder="Role"
            valueEnum={UserRoleEnum}
            rules={[{ required: true, message: "Role is required" }]}
          />
        </Col>
        <Col sm={24} md={12}>
          <ProFormCheckbox name="is_active" label="Active">
            Is Active
          </ProFormCheckbox>
        </Col>
        <Col span={24}>
          {socialProviders.length > 0 && (
            <ProFormList
              label="Social channels"
              name="social_links"
              className="pro-form-list-w-full"
              creatorButtonProps={false}
              deleteIconProps={false}
              copyIconProps={false}
              creatorRecord={{}}
            >
              {(_, index) => {
                const socialProviderId = form.getFieldValue([
                  "social_links",
                  index,
                  "social_provider_id",
                ]);
                const socialProvider = socialProviders.find(
                  (item) => item.id === socialProviderId
                );
                return (
                  <div style={{ width: "100%" }}>
                    <ProFormText
                      style={{ width: "100%" }}
                      name="url"
                      rules={[{ type: "url", message: "must be url" }]}
                      fieldProps={{ addonBefore: socialProvider?.name }}
                    />
                  </div>
                );
              }}
            </ProFormList>
          )}
        </Col>
      </Row>
    </ModalForm>
  );
};

export default Update;
