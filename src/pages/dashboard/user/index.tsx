import {
  DeleteOutlined,
  EditOutlined,
  KeyOutlined,
  MoreOutlined,
} from "@ant-design/icons";
import {
  ProFormDateRangePicker,
  ProFormInstance,
  ProFormSelect,
  ProFormText,
} from "@ant-design/pro-form";
import { ActionType } from "@ant-design/pro-table";
import { useRequest } from "ahooks";
import { Button, Dropdown, Tag, message } from "antd";
import { DownloadButton, StatusTag, UserAvatar } from "components";
import MainTable from "components/main_table";
import { FC, useRef, useState } from "react";
import socialProvider from "services/user/settings/social_provider";
import user from "services/user/user";
import { User, UserRoleEnum } from "services/user/user/types";
import { DefaultPagination, IsBoolEnum } from "types";
import { renderDate, tableCellFixed, tablePagination } from "utils";
import Create from "./create";
import Remove from "./remove";
import Update from "./update";
import ChangePassword from "./change_password";

const Users: FC = () => {
  const actionRef = useRef<ActionType>();
  const fetch = useRequest(user.page, {
    manual: true,
    onError: (err) => {
      message.error(err.message);
    },
  });

  const fetchSocialProviders = useRequest(
    () => socialProvider.page({ is_all: true }),
    {
      onError: (err) => {
        message.error(err.message);
      },
    }
  );

  const [update, setUpdate] = useState<User>();
  const [changePassword, setChangePassword] = useState<User>();
  const [remove, setRemove] = useState<User>();

  const ref = useRef<ProFormInstance>();

  const reload = () => {
    setRemove(undefined);
    setUpdate(undefined);
    setChangePassword(undefined);
    actionRef.current?.reload();
  };

  return (
    <>
      <MainTable<User>
        id="main-table"
        rowKey="id"
        scroll={{ x: "auto" }}
        actionRef={actionRef}
        search={false}
        pagination={DefaultPagination as any}
        headerTitle={
          <div
            style={{
              color: "#344054",
              fontWeight: 600,
              fontSize: 18,
              lineHeight: "28px",
              display: "flex",
              alignItems: "center",
              columnGap: "4px",
            }}
          >
            Users <Tag>{fetch.data?.total || 0}</Tag>
          </div>
        }
        formRef={ref}
        tableAlertRender={false}
        columns={[
          {
            title: "№",
            width: 48,
            fixed: "left",
            dataIndex: "index",
            valueType: "index",
          },
          {
            ...tableCellFixed(200),
            ellipsis: true,
            sorter: true,
            search: false,
            title: "Avatar",
            valueType: "text",
            dataIndex: "avatar_path",
            render: (_, record) => <UserAvatar user={record} />,
          },
          {
            hideInTable: true,
            dataIndex: "first_name",
            filter: (
              <ProFormText
                noStyle
                className="w-full"
                name="first_name"
                placeholder="Search by First name"
              />
            ),
          },
          {
            hideInTable: true,
            dataIndex: "last_name",
            filter: (
              <ProFormText
                noStyle
                className="w-full"
                name="last_name"
                placeholder="Search by Last name"
              />
            ),
          },
          {
            hideInTable: true,
            dataIndex: "position",
            filter: (
              <ProFormText
                noStyle
                className="w-full"
                name="position"
                placeholder="Search by Position"
              />
            ),
          },
          {
            ...tableCellFixed(150),
            ellipsis: true,
            sorter: true,
            title: "Email",
            valueType: "text",
            dataIndex: "email",
            filter: (
              <ProFormText
                noStyle
                className="w-full"
                name="email"
                placeholder="Search by Email"
              />
            ),
          },
          {
            ...tableCellFixed(150),
            ellipsis: true,
            sorter: true,
            title: "Bio",
            valueType: "text",
            dataIndex: "bio",
            filter: (
              <ProFormText
                noStyle
                className="w-full"
                name="bio"
                placeholder="Search by Bio"
              />
            ),
          },
          {
            ...tableCellFixed(200),
            ellipsis: true,
            sorter: true,
            title: "Role",
            valueType: "text",
            valueEnum: UserRoleEnum,
            dataIndex: "role",
            filter: (
              <ProFormSelect
                noStyle
                style={{ width: "100%" }}
                name="role"
                valueEnum={UserRoleEnum}
                placeholder="Search by Role"
              />
            ),
          },
          {
            ...tableCellFixed(100),
            ellipsis: true,
            title: "Is Active",
            dataIndex: "is_active",
            defaultSortOrder: "descend",
            valueType: "dateRange",
            sorter: true,
            filter: (
              <ProFormSelect
                noStyle
                style={{ width: "100%" }}
                name="is_active"
                valueEnum={IsBoolEnum}
                placeholder={"Is active"}
              />
            ),
            render: (_, record) => <StatusTag status={record.is_active} />,
          },
          {
            ...tableCellFixed(100),
            ellipsis: true,
            title: "Created at",
            dataIndex: "created_at",
            defaultSortOrder: "descend",
            valueType: "dateRange",
            sorter: true,
            filter: (
              <ProFormDateRangePicker
                noStyle
                className="w-full"
                name="created_at"
                placeholder={["Start", "End"]}
              />
            ),
            render: (_, record) => renderDate(record.created_at, true),
          },
          {
            ...tableCellFixed(68),
            fixed: "right",
            ellipsis: true,
            title: "Action",
            search: false,
            dataIndex: "action",
            render: (_, record) => (
              <Dropdown
                menu={{
                  items: [
                    {
                      key: "change_password",
                      label: "Change password",
                      icon: <KeyOutlined />,
                      onClick: () => {
                        setChangePassword(record);
                      },
                    },
                    {
                      key: "update",
                      label: "Edit",
                      icon: <EditOutlined />,
                      onClick: () => {
                        setUpdate(record);
                      },
                    },
                    {
                      key: "remove",
                      label: "Remove",
                      danger: true,
                      icon: <DeleteOutlined />,
                      onClick: () => {
                        setRemove(record);
                      },
                    },
                  ],
                }}
                placement="bottomRight"
              >
                <Button style={{ borderColor: "white" }}>
                  <MoreOutlined />
                </Button>
              </Dropdown>
            ),
          },
        ]}
        request={async (params, sort) => {
          const result = await fetch.runAsync({
            ...tablePagination({ ...params }, sort),
            is_all: true,
          });
          return {
            data: result?.items || [],
            total: result?.total || 0,
            success: true,
          };
        }}
        toolbar={{
          multipleLine: true,
          actions: [
            <DownloadButton
              key="download"
              filename="Users"
              formRef={ref.current}
            />,
            <Create
              key="create"
              socialProviders={fetchSocialProviders.data?.items}
              onFinish={() => reload()}
            />,
          ],
        }}
      />
      <Update
        data={update}
        socialProviders={fetchSocialProviders.data?.items}
        onFinish={() => {
          reload();
        }}
        onClose={() => {
          setUpdate(undefined);
        }}
      />
      <ChangePassword
        data={changePassword}
        onFinish={() => {
          reload();
        }}
        onClose={() => {
          setChangePassword(undefined);
        }}
      />
      <Remove
        item={remove}
        onFinish={() => {
          reload();
        }}
        onClose={() => {
          setRemove(undefined);
        }}
      />
    </>
  );
};

export default Users;
