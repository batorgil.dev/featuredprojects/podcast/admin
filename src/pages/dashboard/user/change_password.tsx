import { ModalForm, ProFormText } from "@ant-design/pro-form";
import { useRequest } from "ahooks";
import { Col, Divider, Form, Row, message } from "antd";
import useDrawerWidth from "hooks/useDrawerWidth";
import { FC, useEffect } from "react";
import user from "services/user/user";
import { ChangePasswordAdminInput, User } from "services/user/user/types";

interface Props {
  data?: User;
  onClose: () => void;
  onFinish: () => void;
}

const ChangePassword: FC<Props> = ({ data, onFinish, onClose }) => {
  const [form] = Form.useForm<ChangePasswordAdminInput>();
  const changePassword = useRequest(user.changePassword, {
    manual: true,
    onSuccess: () => {
      message.success({
        content: "Changed password",
      });
    },
    onError: (err) => {
      message.error({ content: err.message, style: { color: "red" } });
    },
  });

  const drawerWidth = useDrawerWidth(620);
  const setFieldsValue = form.setFieldsValue;

  useEffect(() => {
    if (data) {
      setFieldsValue({
        password: "",
        confirm: "",
      });
    }
  }, [data, setFieldsValue]);

  return (
    <ModalForm<ChangePasswordAdminInput>
      title="Change password"
      width={drawerWidth}
      form={form}
      open={!!data}
      onOpenChange={(isOpen) => {
        if (!isOpen) onClose();
      }}
      modalProps={{
        forceRender: true,
        destroyOnClose: true,
      }}
      submitter={{
        searchConfig: {
          resetText: "Back",
          submitText: "Save",
        },
      }}
      onFinish={async (values) => {
        await changePassword.runAsync(data?.id || 0, {
          ...values,
        });
        onFinish();
        return true;
      }}
    >
      <Divider />
      <Row gutter={[12, 12]}>
        <Col sm={24} md={12}>
          <ProFormText.Password
            name="password"
            label="Password"
            placeholder="Password"
            rules={[
              {
                required: true,
                message: "Password is required",
              },
            ]}
          />
        </Col>
        <Col sm={24} md={12}>
          <ProFormText.Password
            name="confirm"
            label="Confirm"
            placeholder="Confirm"
            rules={[
              {
                required: true,
                message: "Confirm is required",
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue("password") === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(
                    new Error("New password and confirm not match!")
                  );
                },
              }),
            ]}
          />
        </Col>
      </Row>
    </ModalForm>
  );
};

export default ChangePassword;
