import {
  ModalForm,
  ProFormDigit,
  ProFormText,
  ProFormTextArea,
  ProFormUploadButton,
} from "@ant-design/pro-form";
import { useRequest } from "ahooks";
import { Divider, Form, message } from "antd";
import useDrawerWidth from "hooks/useDrawerWidth";
import { FC, useEffect } from "react";
import review from "services/user/customer/review";
import { Review, ReviewUpdateInput } from "services/user/customer/review/types";
import file from "services/user/file";

interface Props {
  data?: Review;
  onClose: () => void;
  onFinish: () => void;
}

const Update: FC<Props> = ({ data, onFinish, onClose }) => {
  const [form] = Form.useForm<ReviewUpdateInput>();
  const update = useRequest(review.update, {
    manual: true,
    onSuccess: () => {
      message.success({
        content: "Successfully saved",
      });
    },
    onError: (err) => {
      message.error({ content: err.message, style: { color: "red" } });
    },
  });

  const uploads = useRequest(file.uploads, {
    manual: true,
    onError: (err) => message.error(err.message),
  });

  const drawerWidth = useDrawerWidth(620);
  const setFieldsValue = form.setFieldsValue;

  useEffect(() => {
    if (data) {
      setFieldsValue({
        avatar_path: data.avatar_path,
        fullname: data.fullname,
        comment: data.comment,
        star: data.star,
        image: data.avatar_path
          ? [
              {
                uid: data.avatar_path,
                name: file.getFileName(data.avatar_path),
                status: "done",
                response: "",
                url: file.fileToUrl(data.avatar_path || ""),
              },
            ]
          : [],
      });
    }
  }, [data, setFieldsValue]);

  return (
    <ModalForm<ReviewUpdateInput>
      title="Edit"
      width={drawerWidth}
      form={form}
      open={!!data}
      onOpenChange={(isOpen) => {
        if (!isOpen) onClose();
      }}
      modalProps={{
        forceRender: true,
        destroyOnClose: true,
      }}
      submitter={{
        searchConfig: {
          resetText: "Back",
          submitText: "Save",
        },
      }}
      onFinish={async (values) => {
        if (values.image && values.image.length > 0) {
          const files = values.image;
          const uploadableFiles = file.getUploadableFiles(files);
          const uploadedFile = await uploads.runAsync({
            names: uploadableFiles.map((item) => item.name || "default"),
            files: uploadableFiles,
            bucket_name: "sponsers",
          });
          const uploadedFilePaths = uploadedFile.map((item) => item.path);
          const finalPaths = file.getPaths(
            "sponsers",
            uploadedFilePaths,
            files
          );
          values.avatar_path = finalPaths[0];
        }
        values.image = undefined;

        await update.runAsync(data?.id || 0, {
          ...values,
        });
        onFinish();
        return true;
      }}
    >
      <Divider />
      <ProFormUploadButton
        required
        label="Avatar"
        name="image"
        title="Image insert"
        accept="image/*"
        max={1}
        fieldProps={{
          listType: "picture-card",
          beforeUpload: () => false,
        }}
      />
      <ProFormText
        name="fullname"
        label="Fullname"
        placeholder="Fullname"
        rules={[{ required: true, message: "Fullname is required" }]}
      />
      <ProFormTextArea
        name="comment"
        label="Comment"
        placeholder="Comment"
        rules={[{ required: true, message: "Comment is required" }]}
      />
      <ProFormDigit
        name="star"
        label="Star"
        placeholder="Star"
        max={5}
        min={0}
        rules={[{ required: true, message: "Star between 0 - 5" }]}
      />
    </ModalForm>
  );
};

export default Update;
