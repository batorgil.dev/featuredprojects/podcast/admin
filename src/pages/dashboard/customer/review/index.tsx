import {
  DeleteOutlined,
  EditOutlined,
  MoreOutlined,
  StarFilled,
} from "@ant-design/icons";
import {
  ProFormDateRangePicker,
  ProFormInstance,
  ProFormText,
} from "@ant-design/pro-form";
import { ActionType } from "@ant-design/pro-table";
import { useRequest } from "ahooks";
import { Avatar, Button, Dropdown, Space, Tag, message } from "antd";
import { DownloadButton } from "components";
import MainTable from "components/main_table";
import { FC, useRef, useState } from "react";
import review from "services/user/customer/review";
import { Review } from "services/user/customer/review/types";
import file from "services/user/file";
import { renderDate, tableCellFixed, tablePagination } from "utils";
import Create from "./create";
import Remove from "./remove";
import Update from "./update";
import { DefaultPagination } from "types";

const Reviews: FC = () => {
  const actionRef = useRef<ActionType>();
  const fetch = useRequest(review.page, {
    manual: true,
    onError: (err) => {
      message.error(err.message);
    },
  });

  const [update, setUpdate] = useState<Review>();
  const [remove, setRemove] = useState<Review>();

  const ref = useRef<ProFormInstance>();

  const reload = () => {
    setRemove(undefined);
    setUpdate(undefined);
    actionRef.current?.reload();
  };

  return (
    <>
      <MainTable<Review>
        id="main-table"
        rowKey="id"
        scroll={{ x: "auto" }}
        actionRef={actionRef}
        search={false}
        pagination={DefaultPagination as any}
        headerTitle={
          <div
            style={{
              color: "#344054",
              fontWeight: 600,
              fontSize: 18,
              lineHeight: "28px",
              display: "flex",
              alignItems: "center",
              columnGap: "4px",
            }}
          >
            Reviews <Tag>{fetch.data?.total || 0}</Tag>
          </div>
        }
        formRef={ref}
        tableAlertRender={false}
        columns={[
          {
            title: "№",
            width: 48,
            fixed: "left",
            dataIndex: "index",
            valueType: "index",
          },
          {
            ...tableCellFixed(80),
            ellipsis: true,
            sorter: true,
            search: false,
            title: "Avatar",
            valueType: "text",
            dataIndex: "avatar_path",
            render: (_, record) => (
              <Avatar src={file.fileToUrl(record.avatar_path)} />
            ),
          },
          {
            ...tableCellFixed(200),
            ellipsis: true,
            sorter: true,
            title: "Fullname",
            valueType: "text",
            dataIndex: "fullname",
            filter: (
              <ProFormText
                noStyle
                className="w-full"
                name="fullname"
                placeholder="Search by Name"
              />
            ),
          },
          {
            ...tableCellFixed(200),
            ellipsis: true,
            sorter: true,
            title: "Comment",
            valueType: "text",
            dataIndex: "comment",
            filter: (
              <ProFormText
                noStyle
                className="w-full"
                name="comment"
                placeholder="Search by comment"
              />
            ),
          },
          {
            ...tableCellFixed(200),
            ellipsis: true,
            sorter: true,
            title: "Star",
            valueType: "text",
            dataIndex: "star",
            render: (_, record) => {
              const stars = new Array(record.star).fill(0);
              return (
                <Space>
                  {stars.map((_, index) => (
                    <StarFilled key={index} style={{ color: "#1ad993" }} />
                  ))}
                </Space>
              );
            },
          },
          {
            ...tableCellFixed(100),
            ellipsis: true,
            title: "Created at",
            dataIndex: "created_at",
            defaultSortOrder: "descend",
            valueType: "dateRange",
            sorter: true,
            filter: (
              <ProFormDateRangePicker
                noStyle
                className="w-full"
                name="created_at"
                placeholder={["Start", "End"]}
              />
            ),
            render: (_, record) => renderDate(record.created_at, true),
          },
          {
            ...tableCellFixed(68),
            fixed: "right",
            ellipsis: true,
            title: "Action",
            search: false,
            dataIndex: "action",
            render: (_, record) => (
              <Dropdown
                menu={{
                  items: [
                    {
                      key: "update",
                      label: "Edit",
                      icon: <EditOutlined />,
                      onClick: () => {
                        setUpdate(record);
                      },
                    },
                    {
                      key: "remove",
                      label: "Remove",
                      danger: true,
                      icon: <DeleteOutlined />,
                      onClick: () => {
                        setRemove(record);
                      },
                    },
                  ],
                }}
                placement="bottomRight"
              >
                <Button style={{ borderColor: "white" }}>
                  <MoreOutlined />
                </Button>
              </Dropdown>
            ),
          },
        ]}
        request={async (params, sort) => {
          const result = await fetch.runAsync({
            ...tablePagination({ ...params }, sort),
            is_all: true,
          });
          return {
            data: result?.items || [],
            total: result?.total || 0,
            success: true,
          };
        }}
        toolbar={{
          multipleLine: true,
          actions: [
            <DownloadButton
              key="download"
              filename="Reviews"
              formRef={ref.current}
            />,
            <Create key="create" onFinish={() => reload()} />,
          ],
        }}
      />
      <Update
        data={update}
        onFinish={() => {
          reload();
        }}
        onClose={() => {
          setUpdate(undefined);
        }}
      />
      <Remove
        item={remove}
        onFinish={() => {
          reload();
        }}
        onClose={() => {
          setRemove(undefined);
        }}
      />
    </>
  );
};

export default Reviews;
