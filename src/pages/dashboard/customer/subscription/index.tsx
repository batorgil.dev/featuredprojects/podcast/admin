import { DeleteOutlined, MoreOutlined } from "@ant-design/icons";
import {
  ProFormDateRangePicker,
  ProFormInstance,
  ProFormText,
} from "@ant-design/pro-form";
import { ActionType } from "@ant-design/pro-table";
import { useRequest } from "ahooks";
import { Button, Dropdown, Tag, message } from "antd";
import { DownloadButton } from "components";
import MainTable from "components/main_table";
import { FC, useRef, useState } from "react";
import { renderDate, tableCellFixed, tablePagination } from "utils";
import Remove from "./remove";
import subscription from "services/user/customer/subscription";
import { Subscription } from "services/user/customer/subscription/types";
import { DefaultPagination } from "types";

const Subscriptions: FC = () => {
  const actionRef = useRef<ActionType>();
  const fetch = useRequest(subscription.page, {
    manual: true,
    onError: (err) => {
      message.error(err.message);
    },
  });

  const [remove, setRemove] = useState<Subscription>();

  const ref = useRef<ProFormInstance>();

  const reload = () => {
    setRemove(undefined);
    actionRef.current?.reload();
  };

  return (
    <>
      <MainTable<Subscription>
        id="main-table"
        rowKey="id"
        scroll={{ x: "auto" }}
        actionRef={actionRef}
        search={false}
        pagination={DefaultPagination as any}
        headerTitle={
          <div
            style={{
              color: "#344054",
              fontWeight: 600,
              fontSize: 18,
              lineHeight: "28px",
              display: "flex",
              alignItems: "center",
              columnGap: "4px",
            }}
          >
            Subscription <Tag>{fetch.data?.total || 0}</Tag>
          </div>
        }
        formRef={ref}
        tableAlertRender={false}
        columns={[
          {
            title: "№",
            width: 48,
            fixed: "left",
            dataIndex: "index",
            valueType: "index",
          },
          {
            ...tableCellFixed(200),
            ellipsis: true,
            sorter: true,
            title: "Email",
            valueType: "text",
            dataIndex: "email",
            filter: (
              <ProFormText
                noStyle
                className="w-full"
                name="email"
                placeholder="Search by Email"
              />
            ),
          },
          {
            ...tableCellFixed(100),
            ellipsis: true,
            title: "Created at",
            dataIndex: "created_at",
            defaultSortOrder: "descend",
            valueType: "dateRange",
            sorter: true,
            filter: (
              <ProFormDateRangePicker
                noStyle
                className="w-full"
                name="created_at"
                placeholder={["Start", "End"]}
              />
            ),
            render: (_, record) => renderDate(record.created_at, true),
          },
          {
            ...tableCellFixed(68),
            fixed: "right",
            ellipsis: true,
            title: "Action",
            search: false,
            dataIndex: "action",
            render: (_, record) => (
              <Dropdown
                menu={{
                  items: [
                    {
                      key: "remove",
                      label: "Remove",
                      danger: true,
                      icon: <DeleteOutlined />,
                      onClick: () => {
                        setRemove(record);
                      },
                    },
                  ],
                }}
                placement="bottomRight"
              >
                <Button style={{ borderColor: "white" }}>
                  <MoreOutlined />
                </Button>
              </Dropdown>
            ),
          },
        ]}
        request={async (params, sort) => {
          const result = await fetch.runAsync({
            ...tablePagination({ ...params }, sort),
            is_all: true,
          });
          return {
            data: result?.items || [],
            total: result?.total || 0,
            success: true,
          };
        }}
        toolbar={{
          multipleLine: true,
          actions: [
            <DownloadButton
              key="download"
              filename="Subscription"
              formRef={ref.current}
            />,
          ],
        }}
      />
      <Remove
        item={remove}
        onFinish={() => {
          reload();
        }}
        onClose={() => {
          setRemove(undefined);
        }}
      />
    </>
  );
};

export default Subscriptions;
