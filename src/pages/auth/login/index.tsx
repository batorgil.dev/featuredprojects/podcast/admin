import { LockOutlined, MailOutlined } from "@ant-design/icons";
import {
  LoginForm,
  ProFormCheckbox,
  ProFormInstance,
  ProFormText,
} from "@ant-design/pro-form";
import { useRequest } from "ahooks";
import { Button, message } from "antd";
import { authAtom } from "atoms/auth";
import { useAtom } from "jotai";
import { FC, useRef } from "react";
import { useNavigate } from "react-router";
import auth from "services/user/auth";
import { LoginData } from "services/user/auth/types";

const Login: FC = () => {
  const navigation = useNavigate();
  const [, setAuth] = useAtom(authAtom);
  const userData = auth.getRememberUser();
  const formRef = useRef<ProFormInstance<LoginData>>();

  const login = useRequest(auth.login, {
    manual: true,
    onSuccess: async (data, [values]) => {
      await auth.rememberUser(values);
      await auth.saveToken(data.token);
      setAuth(data.user);
      message.success({
        content: "Successful login",
      });
      navigation("/dashboard/content/blog");
    },
    onError: (err) => {
      message.error({ content: err.message, style: { color: "red" } });
    },
  });

  return (
    <LoginForm<LoginData>
      formRef={formRef}
      onFinish={async (data: LoginData) => {
        await login.run(data);
      }}
      initialValues={userData}
      submitter={{
        render: () => (
          <Button
            block
            size="large"
            type="primary"
            htmlType="submit"
            loading={login.loading}
            style={{
              border: "none",
              borderRadius: 12,
            }}
          >
            Login
          </Button>
        ),
      }}
      contentStyle={{
        margin: "auto",
        padding: "10px 20px",
        minWidth: "266px",
        background: "white",
        borderRadius: 8,
      }}
    >
      <div
        style={{
          width: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          marginBottom: 20,
        }}
      >
        <img
          src="/logo.svg"
          alt="logo"
          style={{
            width: "70px",
          }}
        />
      </div>

      <ProFormText
        width="lg"
        name="email"
        required={false}
        placeholder="Email"
        fieldProps={{
          size: "large",
          prefix: <MailOutlined style={{ color: "#667085" }} />,
        }}
        rules={[
          {
            required: true,
            message: "Email is required",
          },
        ]}
      />
      <ProFormText.Password
        width="lg"
        name="password"
        required={false}
        placeholder="Password"
        fieldProps={{
          size: "large",
          prefix: <LockOutlined style={{ color: "#667085" }} />,
        }}
        rules={[
          {
            required: true,
            message: "Password is required",
          },
        ]}
      />
      <ProFormCheckbox name="remember">Email remember</ProFormCheckbox>
    </LoginForm>
  );
};

export default Login;
