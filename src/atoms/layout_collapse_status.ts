import { atom } from "jotai";

export const layoutCollapse = atom<boolean>(false);
