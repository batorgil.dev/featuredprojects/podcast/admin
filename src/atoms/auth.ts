import { atomWithStorage } from "jotai/utils";
import { User } from "services/user/user/types";

export const authAtom = atomWithStorage<User | undefined>("token", undefined);
