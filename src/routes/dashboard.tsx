import { lazy } from "react";
import { IRoute } from "./types";

const NotFound = lazy(() => import("pages/exceptions/NotFound"));

// Settings
const Categories = lazy(() => import("pages/dashboard/settings/category"));
const Sponsers = lazy(() => import("pages/dashboard/settings/sponser"));
const PodcastProviders = lazy(
  () => import("pages/dashboard/settings/podcast_provider")
);
const SocialProviders = lazy(
  () => import("pages/dashboard/settings/social_provider")
);

// Customer
const Reviews = lazy(() => import("pages/dashboard/customer/review"));
const Subscriptions = lazy(
  () => import("pages/dashboard/customer/subscription")
);
const ContactRequests = lazy(() => import("pages/dashboard/customer/contact"));

// Content
const Blogs = lazy(() => import("pages/dashboard/content/blog"));
const BlogCreate = lazy(() => import("pages/dashboard/content/blog/create"));
const BlogUpdate = lazy(() => import("pages/dashboard/content/blog/update"));
const BlogView = lazy(() => import("pages/dashboard/content/blog/view"));

const Podcasts = lazy(() => import("pages/dashboard/content/podcast"));
const PodcastCreate = lazy(
  () => import("pages/dashboard/content/podcast/create")
);
const PodcastUpdate = lazy(
  () => import("pages/dashboard/content/podcast/update")
);
const PodcastView = lazy(() => import("pages/dashboard/content/podcast/view"));

// User
const Users = lazy(() => import("pages/dashboard/user"));

const dashboardRoutes: IRoute[] = [
  {
    key: "not_found",
    path: "*",
    component: <NotFound />,
  },
  {
    key: "settings/category",
    path: "settings/category",
    component: <Categories />,
  },
  {
    key: "settings/sponser",
    path: "settings/sponser",
    component: <Sponsers />,
  },
  {
    key: "settings/podcast_provider",
    path: "settings/podcast_provider",
    component: <PodcastProviders />,
  },
  {
    key: "settings/social_provider",
    path: "settings/social_provider",
    component: <SocialProviders />,
  },
  {
    key: "customer/review",
    path: "customer/review",
    component: <Reviews />,
  },
  {
    key: "customer/subscription",
    path: "customer/subscription",
    component: <Subscriptions />,
  },
  {
    key: "customer/contact",
    path: "customer/contact",
    component: <ContactRequests />,
  },
  {
    key: "content/blog",
    path: "content/blog",
    component: <Blogs />,
  },
  {
    key: "content/blog/create",
    path: "content/blog/create",
    component: <BlogCreate />,
  },
  {
    key: "content/blog/update/:id",
    path: "content/blog/update/:id",
    component: <BlogUpdate />,
  },
  {
    key: "content/blog/view/:id",
    path: "content/blog/view/:id",
    component: <BlogView />,
  },
  {
    key: "content/podcast",
    path: "content/podcast",
    component: <Podcasts />,
  },
  {
    key: "content/podcast/create",
    path: "content/podcast/create",
    component: <PodcastCreate />,
  },
  {
    key: "content/podcast/update/:id",
    path: "content/podcast/update/:id",
    component: <PodcastUpdate />,
  },
  {
    key: "content/podcast/view/:id",
    path: "content/podcast/view/:id",
    component: <PodcastView />,
  },
  {
    key: "user",
    path: "user",
    component: <Users />,
  },
];
export default dashboardRoutes;
