interface ImportMetaEnv {
  VITE_WS_URL: string;
  VITE_API_URL: string;
  VITE_FILE_GET_URL: string;
}
