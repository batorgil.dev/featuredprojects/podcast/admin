import ProLayout from "@ant-design/pro-layout";
import { layoutCollapse } from "atoms/layout_collapse_status";
import { Logo } from "components";
import { useAtom } from "jotai";
import { FC } from "react";
import { Outlet, useNavigate } from "react-router-dom";
import Footer from "./footer";
import LayoutFooter from "./layout_footer";
import { menuData, menuItemRender } from "./menu";

const DashboardLayout: FC = () => {
  const navigate = useNavigate();

  const [collapsed, setCollapsed] = useAtom(layoutCollapse);

  return (
    <div id="pro-layout">
      <ProLayout
        disableMobile={false}
        collapsed={collapsed}
        onCollapse={setCollapsed}
        onMenuHeaderClick={() => navigate("/")}
        menuHeaderRender={() => <Logo />}
        menuDataRender={() => menuData}
        menuItemRender={menuItemRender}
        menuFooterRender={() => <LayoutFooter />}
        contentStyle={{
          margin: 0,
          overflowY: "auto",
          background: "#e8e8e8",
          padding: "1.628rem",
          height: "calc(100vh - 48px)",
        }}
        layout="side"
        siderWidth={208}
        navTheme="light"
        theme="dark"
        colorWeak={false}
        fixedHeader={true}
        fixSiderbar={true}
        contentWidth="Fluid"
        footerRender={() => <Footer />}
      >
        <Outlet />
      </ProLayout>
    </div>
  );
};

export default DashboardLayout;
