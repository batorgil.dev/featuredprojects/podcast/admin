import {
  PlayCircleOutlined,
  SettingOutlined,
  StarOutlined,
  UserAddOutlined,
} from "@ant-design/icons";
import { MenuDataItem } from "@ant-design/pro-layout";
import { Link } from "react-router-dom";

export const menuItemRender: (
  item: MenuDataItem & {
    isUrl: boolean;
    onClick: () => void;
  },
  defaultDom: React.ReactNode,
  menuProps: any
) => React.ReactNode = (menuItemProps, defaultDom) =>
  menuItemProps.isUrl ? (
    defaultDom
  ) : (
    <Link to={menuItemProps.path || "/"}>{defaultDom}</Link>
  );

export const fontSize = 12;

export const menuData: MenuDataItem[] = [
  {
    key: "/dashboard/content",
    name: "Content",
    icon: <PlayCircleOutlined />,
    path: "/dashboard/content",
    children: [
      {
        key: "/dashboard/content/blog",
        name: "Blog",
        path: "/dashboard/content/blog",
      },
      {
        key: "/dashboard/content/podcast",
        name: "Podcast",
        path: "/dashboard/content/podcast",
      },
    ],
  },
  {
    key: "/dashboard/settings",
    name: "Settings",
    path: "/dashboard/settings",
    icon: <SettingOutlined />,
    children: [
      {
        key: "/dashboard/settings/category",
        name: "Categories",
        path: "/dashboard/settings/category",
      },
      {
        key: "/dashboard/settings/sponser",
        name: "Sponsers",
        path: "/dashboard/settings/sponser",
      },
      {
        key: "/dashboard/settings/podcast_provider",
        name: "Podcast accounts",
        path: "/dashboard/settings/podcast_provider",
      },
      {
        key: "/dashboard/settings/social_provider",
        name: "Social channels",
        path: "/dashboard/settings/social_provider",
      },
    ],
  },
  {
    key: "/dashboard/customer",
    name: "Customer",
    icon: <StarOutlined />,
    path: "/dashboard/customer",
    children: [
      {
        key: "/dashboard/customer/review",
        name: "Review",
        path: "/dashboard/customer/review",
      },
      {
        key: "/dashboard/customer/subscription",
        name: "Subscription",
        path: "/dashboard/customer/subscription",
      },
      {
        key: "/dashboard/customer/contact",
        name: "Contact requests",
        path: "/dashboard/customer/contact",
      },
    ],
  },
  {
    key: "/dashboard/user",
    name: "User",
    icon: <UserAddOutlined />,
    path: "/dashboard/user",
  },
];
