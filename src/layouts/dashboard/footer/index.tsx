import { Col, Row } from "antd";
import { FC } from "react";

const Footer: FC = () => (
  <div className="footer">
    <Row
      justify="center"
      style={{
        bottom: 10,
        padding: "16px 32px",
        background: "#FFFFFF",
      }}
    >
      <div className="row-title">
        <Col>
          <span
            style={{
              fontSize: "12px",
              textAlign: "center",
              color: "#667085",
              fontWeight: 500,
            }}
          >
            ©{new Date().getFullYear()} |{" "}
            <a
              style={{ cursor: "pointer", color: "#667085" }}
              href="https://batorgil.dev"
              target="_blank"
              rel="noreferrer"
            >
              Powered By Batorgil
            </a>
          </span>
        </Col>
      </div>
    </Row>
  </div>
);

export default Footer;
