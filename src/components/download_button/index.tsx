import { CloudDownloadOutlined } from "@ant-design/icons";
import { ProFormInstance } from "@ant-design/pro-form";
import { Button, Tooltip } from "antd";
import { FC } from "react";
import { renderDate } from "utils";
import { download } from "utils/csv";

interface Props {
  filename: string;
  formRef?: ProFormInstance<any> | undefined;
}

const DownloadButton: FC<Props> = ({ filename, formRef }) => {
  return (
    <Tooltip title="Excel татах">
      <Button
        key="button"
        type="primary"
        size="middle"
        icon={<CloudDownloadOutlined />}
        style={{
          background: "white",
          borderColor: "lightGray",
          color: "#344054",
        }}
        onClick={() => {
          if (formRef?.getFieldValue("created_at")) {
            let value = formRef
              ?.getFieldValue("created_at")
              .map((item: any) => renderDate(item, true));
            download(
              [filename, value],
              window.document.getElementById("main-table") as HTMLElement
            );
            return;
          }
          download(
            [filename],
            window.document.getElementById("main-table") as HTMLElement
          );
        }}
      >
        {" "}
        Download
      </Button>
    </Tooltip>
  );
};

export default DownloadButton;
