import { SubmitterProps } from "@ant-design/pro-components";

import { Button, FormInstance } from "antd";
import { ArrowRightOutlined, PlusCircleOutlined } from "@ant-design/icons";

const StepFormSubmitter = (stepCount: number, cancel?: Function) => {
  const submitter: SubmitterProps<{
    step: number;
    onPre: () => void;
    form?: FormInstance<any> | undefined;
  }> = {
    render: (props: any) => {
      switch (props.step) {
        case 0:
          return (
            <div>
              <Button key="pre" size="middle" onClick={() => cancel?.()}>
                Цуцлах
              </Button>
              <Button
                key="next"
                type="primary"
                size="middle"
                style={{ marginLeft: 10 }}
                onClick={() => props.onSubmit?.()}
              >
                Үргэлжлүүлэх <ArrowRightOutlined />
              </Button>
            </div>
          );
        case stepCount - 1:
          return (
            <div>
              <Button key="pre" size="middle" onClick={() => props.onPre?.()}>
                Back
              </Button>
              <Button
                key="save"
                type="primary"
                size="middle"
                style={{ marginLeft: 10 }}
                onClick={() => props.onSubmit?.()}
              >
                <PlusCircleOutlined />
                Бүртгэх
              </Button>
            </div>
          );
        default:
          return (
            <div>
              <Button key="pre" size="middle" onClick={() => props.onPre?.()}>
                Back
              </Button>
              <Button
                key="next"
                type="primary"
                size="middle"
                style={{ marginLeft: 10 }}
                onClick={() => props.onSubmit?.()}
              >
                Үргэлжлүүлэх
              </Button>
            </div>
          );
      }
    },
  };
  return submitter;
};

export default StepFormSubmitter;
