import { useRequest } from "ahooks";
import { message } from "antd";
import BraftEditor from "braft-editor";
import { FC, useEffect } from "react";
import file from "services/user/file";

const BraftEditorPropStated: FC<{
  initialValue?: string;
  disabled?: boolean;
  data: any;
  setData: (data: any) => void;
}> = ({ data, initialValue, disabled = false, setData }) => {
  useEffect(() => {
    if (initialValue) {
      setData(BraftEditor.createEditorState(initialValue || ""));
    }
  }, [initialValue, setData]);

  const uploads = useRequest(file.uploads, {
    manual: true,
    onError: (err) => message.error(err.message),
  });

  return (
    <BraftEditor
      value={data}
      onChange={disabled ? () => {} : setData}
      media={{
        validateFn: () => {
          return true;
        },
        uploadFn: async ({ file: rawFile, success, error }) => {
          try {
            const res = await uploads.runAsync({
              files: [rawFile],
              names: ["image"],
              bucket_name: "content",
            });

            success({
              url: file.fileToUrl(res[0].path),
              meta: {
                id: res[0].path,
                alt: rawFile.name,
                autoPlay: false,
                controls: true,
                loop: false,
                poster: "",
                title: rawFile.name,
              },
            });
          } catch (err: any) {
            error(err.message);
          }
        },
      }}
      style={{ border: "1px solid #d9d9d9", borderRadius: "6px" }}
      language="en"
    />
  );
};

export default BraftEditorPropStated;
