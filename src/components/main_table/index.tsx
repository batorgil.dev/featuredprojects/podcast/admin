import { FilterOutlined } from "@ant-design/icons";
import {
  ParamsType,
  ProColumns,
  ProForm,
  ProTable,
  ProTableProps,
} from "@ant-design/pro-components";
import { useDebounceFn } from "ahooks";
import { Button, Col, Form, Row } from "antd";
import { useCallback, useState } from "react";
import { DefaultPagination } from "types";

export type ProCustomColumn<T, ValueType = "text"> = ProColumns<
  T,
  ValueType
> & {
  filter?: any;
};
export type ProCustomColumns<T, ValueType> = ProCustomColumn<T, ValueType>[];
type ProTableCustomizedProps<DataType, Params, ValueType> = Omit<
  ProTableProps<DataType, Params, ValueType>,
  "columns"
> & { columns: ProCustomColumns<DataType, ValueType> };

function MainTable<
  DataType extends Record<string, any>,
  Params extends ParamsType = ParamsType,
  ValueType = "text"
>({
  columns,
  toolbar,
  ...props
}: ProTableCustomizedProps<DataType, Params, ValueType>) {
  const [expandToolbar, setExpandToolbar] = useState(false);
  const [form] = Form.useForm();

  const { run: filterRun } = useDebounceFn(
    () => {
      (props.actionRef as any)?.current?.reload();
    },
    { wait: 500 }
  );

  const request = useCallback(() => {
    if (props.request) {
      return async (params: any, sort: any) => {
        const values = form.getFieldsValue() || {};
        if (props.request) {
          return props.request({ ...params, ...values } as any, sort, {});
        }
      };
    }
    return undefined;
  }, [props.request]);

  return (
    <ProForm
      size="middle"
      form={form}
      onValuesChange={filterRun}
      submitter={{
        render: () => undefined,
      }}
    >
      <ProTable<DataType, Params, ValueType>
        {...(props as any)}
        toolbar={{
          ...toolbar,
          filter: expandToolbar && (
            <Row style={{ padding: "12px 0" }} gutter={[12, 12]}>
              {columns
                .filter((item) => !!item.filter)
                .map((item, key) => (
                  <Col
                    key={key}
                    xs={24}
                    sm={12}
                    md={8}
                    lg={4}
                    xl={4}
                    xxl={4}
                    style={{ width: "100%", minWidth: "250px" }}
                  >
                    {item.filter}
                  </Col>
                ))}
            </Row>
          ),
          actions: [
            <Button
              style={{
                backgroundColor: "white",
                border: "1px solid #EAECF0",
                color: "#344054",
                // width: "40px",
              }}
              size="middle"
              key={"expand"}
              type="primary"
              onClick={() => setExpandToolbar(!expandToolbar)}
            >
              {expandToolbar}
              <FilterOutlined />
            </Button>,
            ...(toolbar?.actions || []),
          ],
        }}
        pagination={props.pagination === false ? false : DefaultPagination}
        columns={columns?.map(
          ({ title, sorter, defaultSortOrder, ...item }) => {
            return {
              ...item,
              title: title,
              sorter,
              defaultSortOrder,
            };
          }
        )}
        request={request()}
      />
    </ProForm>
  );
}

export default MainTable;
