import { CheckCircleFilled, CloseCircleFilled } from "@ant-design/icons";
import { Tag } from "antd";
import { FC, ReactNode } from "react";
interface Props {
  status: boolean | { label: ReactNode; color: string };
  icon?: ReactNode;
  showIcon?: boolean;
  labels?: { true?: string; false?: string };
}

const StatusTag: FC<Props> = ({ status, icon, labels, showIcon = true }) => {
  const getLabel = (): string => {
    if (status) {
      return labels?.true || "True";
    }
    return labels?.false || "False";
  };

  switch (typeof status) {
    case "boolean":
      return (
        <div className="status">
          <Tag
            icon={
              icon ||
              (showIcon &&
                (status ? (
                  <CheckCircleFilled color="green" />
                ) : (
                  <CloseCircleFilled color="red" />
                )))
            }
            style={{ fontWeight: 550, display: "flex", minWidth: 40 }}
            color={status ? "green" : "red"}
            className="m-0"
          >
            {getLabel()}
          </Tag>
        </div>
      );
    case "object":
      return (
        <Tag icon={icon} color={status.color} className="m-0">
          {status.label}
        </Tag>
      );
    default:
      return null;
  }
};

export default StatusTag;
