import { Space } from "antd";
import { layoutCollapse } from "atoms/layout_collapse_status";
import { useAtom } from "jotai";
import { FC } from "react";
import LogoSVG from "logo.svg?react";
import styles from "./styles.module.less";

interface Props {
  url?: string;
}

const Logo: FC<Props> = () => {
  const [collapsed] = useAtom(layoutCollapse);

  return (
    <Space className={styles.container}>
      <div
        style={{
          display: "flex",
          width: "100%",
          height: "100%",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <LogoSVG width={40} height={40} />
      </div>
      {!collapsed && <div>Podcast</div>}
    </Space>
  );
};

export default Logo;
