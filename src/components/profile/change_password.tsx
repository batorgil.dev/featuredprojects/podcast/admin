import { KeyOutlined } from "@ant-design/icons";
import { ModalForm, ProFormText } from "@ant-design/pro-form";
import { useRequest } from "ahooks";
import { Divider, Form, message } from "antd";
import useDrawerWidth from "hooks/useDrawerWidth";
import { FC } from "react";
import auth from "services/user/auth";
import { ChangePasswordInput } from "services/user/auth/types";
interface Props {
  onFinish: () => void;
}

const ChangePassword: FC<Props> = ({ onFinish }) => {
  const [form] = Form.useForm<ChangePasswordInput>();
  const changePassword = useRequest(auth.changePassword, {
    manual: true,
    onSuccess: () => {
      message.success({
        content: "Successfully saved",
      });
    },
    onError: (err) => {
      message.error({ content: err.message, style: { color: "red" } });
    },
  });

  const drawerWidth = useDrawerWidth(620);

  return (
    <ModalForm<ChangePasswordInput>
      title="Change password"
      width={drawerWidth}
      form={form}
      trigger={
        <div
          onClick={() => {
            form.resetFields();
          }}
        >
          <KeyOutlined /> Change password
        </div>
      }
      modalProps={{
        forceRender: true,
        destroyOnClose: true,
      }}
      submitter={{
        searchConfig: {
          resetText: "Back",
          submitText: "Save",
        },
      }}
      onFinish={async (values) => {
        // console.log(values);
        await changePassword.run({
          ...values,
        });
        onFinish();
        return true;
      }}
    >
      <Divider />
      <ProFormText.Password
        name="old_password"
        label="Old password"
        placeholder="Old password"
        rules={[
          {
            required: true,
            message: "Old password is required",
          },
        ]}
      />
      <ProFormText.Password
        name="password"
        label="New password"
        placeholder="New password"
        rules={[
          {
            required: true,
            message: "New password is required",
          },
        ]}
      />
      <ProFormText.Password
        name="confirm"
        label="Confirm"
        placeholder="Confirm"
        rules={[
          {
            required: true,
            message: "Confirm is required",
          },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (!value || getFieldValue("password") === value) {
                return Promise.resolve();
              }
              return Promise.reject(
                new Error("New password and confirm not match!")
              );
            },
          }),
        ]}
      />
    </ModalForm>
  );
};

export default ChangePassword;
