import { UserOutlined } from "@ant-design/icons";
import {
  ModalForm,
  ProFormText,
  ProFormUploadButton,
  ProFormTextArea,
} from "@ant-design/pro-form";
import { useRequest } from "ahooks";
import { Col, Divider, Form, Row, message } from "antd";
import { authAtom } from "atoms/auth";
import useDrawerWidth from "hooks/useDrawerWidth";
import { useAtom } from "jotai";
import { FC } from "react";
import file from "services/user/file";
import auth from "services/user/auth";
import { InfoUpdateInput } from "services/user/auth/types";

interface Props {
  onFinish: () => void;
}

const ChangeInfo: FC<Props> = ({ onFinish }) => {
  const [form] = Form.useForm<InfoUpdateInput>();
  const [currentUser, setAuth] = useAtom(authAtom);

  const uploads = useRequest(file.uploads, {
    manual: true,
    onError: (err) => message.error(err.message),
  });

  const { run } = useRequest(auth.info, {
    manual: true,
    onSuccess: (user) => {
      setAuth(user);
    },
  });

  const update = useRequest(auth.update, {
    manual: true,
    onSuccess: () => {
      message.success({
        content: "Successfully saved",
      });
      run();
    },
    onError: (err) => {
      message.error({ content: err.message, style: { color: "red" } });
    },
  });

  const { setFieldsValue } = form;

  const drawerWidth = useDrawerWidth(620);

  return (
    <ModalForm<InfoUpdateInput>
      title="Update personal information"
      width={drawerWidth}
      form={form}
      trigger={
        <div
          onClick={() => {
            form.resetFields();
            if (currentUser) {
              setFieldsValue({
                avatar_path: currentUser.avatar_path,
                email: currentUser.email,
                first_name: currentUser.first_name,
                last_name: currentUser.last_name,
                bio: currentUser.bio,
                avatar: currentUser?.avatar_path
                  ? [
                      {
                        uid: currentUser?.avatar_path,
                        name: file.getFileName(currentUser?.avatar_path),
                        status: "done",
                        response: "",
                        url: file.fileToUrl(currentUser?.avatar_path || ""),
                      },
                    ]
                  : [],
              });
            }
          }}
        >
          <UserOutlined /> Update personal information
        </div>
      }
      modalProps={{
        forceRender: true,
        destroyOnClose: true,
      }}
      submitter={{
        searchConfig: {
          resetText: "Back",
          submitText: "Save",
        },
      }}
      onFinish={async (values) => {
        if (values.avatar && values.avatar.length > 0) {
          const files = values.avatar;
          const uploadableFiles = file.getUploadableFiles(files);
          const uploadedFile = await uploads.runAsync({
            names: uploadableFiles.map((item) => item.name || "default"),
            files: uploadableFiles,
            bucket_name: "avatars",
          });
          const uploadedFilePaths = uploadedFile.map((item) => item.path);
          const finalPaths = file.getPaths("avatars", uploadedFilePaths, files);
          values.avatar_path = finalPaths[0];
        }
        values.avatar = undefined;
        await update.runAsync({
          ...values,
        });
        onFinish();
        return true;
      }}
    >
      <Divider />
      <ProFormUploadButton
        required
        label="Avatar"
        name="avatar"
        title="Image insert"
        accept="image/*"
        max={1}
        fieldProps={{
          listType: "picture-card",
          beforeUpload: () => false,
        }}
      />
      <Row gutter={[12, 0]}>
        <Col span={12}>
          <ProFormText
            name="last_name"
            label="Last name"
            placeholder="Last name"
            rules={[
              {
                required: true,
                message: "Last name is required",
              },
            ]}
          />
        </Col>
        <Col span={12}>
          <ProFormText
            name="first_name"
            label="First name"
            placeholder="First name"
            rules={[
              {
                required: true,
                message: "First name is required",
              },
            ]}
          />
        </Col>
        <Col span={12}>
          <ProFormText
            name="email"
            label="Email"
            placeholder="Email"
            rules={[
              {
                required: true,
                type: "email",
                message: "Email is required",
              },
            ]}
          />
        </Col>
        <Col span={24}>
          <ProFormTextArea
            name="bio"
            label="Bio"
            placeholder="Bio"
            rules={[
              {
                required: true,
                message: "Bio is required",
              },
            ]}
          />
        </Col>
      </Row>
    </ModalForm>
  );
};

export default ChangeInfo;
