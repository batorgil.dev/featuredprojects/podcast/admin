import { EllipsisOutlined, LockOutlined } from "@ant-design/icons";
import { Avatar, Button, Dropdown } from "antd";
import { MenuProps } from "antd/lib/menu";
import { authAtom } from "atoms/auth";
import { layoutCollapse } from "atoms/layout_collapse_status";
import { useAtom } from "jotai";
import { FC } from "react";
import { useNavigate } from "react-router";
import auth from "services/user/auth";
import file from "services/user/file";
import { renderFullname } from "utils";
import ChangeInfo from "./change_info";
import ChangePassword from "./change_password";
import styles from "./styles.module.less";

const Profile: FC = () => {
  const navigate = useNavigate();
  const [currentUser, setAuth] = useAtom(authAtom);
  const { email } = currentUser || { email: "" };
  const [collapsed] = useAtom(layoutCollapse);

  const logout = () => {
    setAuth(undefined);
    auth.remToken();
    navigate("/");
  };

  const color = "#4E5BA6";
  const avatar = email?.substring(0, 2) || "AU";

  const menuItems: MenuProps["items"] = [
    {
      key: "1",
      label: <ChangeInfo onFinish={() => {}} />,
    },
    {
      key: "2",
      label: <ChangePassword onFinish={() => {}} />,
    },
    {
      key: "3",
      label: (
        <>
          <LockOutlined /> Logout
        </>
      ),
      danger: true,
      onClick: logout,
    },
  ];

  return (
    <div
      style={{
        display: "flex",
        width: "100%",
        padding: "16px 0 0 0",
        borderTop: "1px solid #EAECF0",
        justifyContent: "space-between",
      }}
    >
      {!collapsed && (
        <div
          style={{
            display: "flex",
          }}
        >
          <Avatar
            shape="circle"
            src={
              currentUser?.avatar_path
                ? file.fileToUrl(currentUser?.avatar_path)
                : undefined
            }
            style={{
              backgroundColor: color,
              cursor: "pointer",
              userSelect: "none",
            }}
          >
            {avatar.toUpperCase()}
          </Avatar>
          <div style={{ marginLeft: 8, color: "#344054" }}>
            <div>
              <strong>{renderFullname(currentUser)}</strong>
            </div>
            <div>{currentUser?.position}</div>
          </div>
        </div>
      )}

      <Dropdown
        menu={{ items: menuItems }}
        overlayClassName={styles.dropdown}
        trigger={["click"]}
      >
        <Button type="text" icon={<EllipsisOutlined />} />
      </Dropdown>
    </div>
  );
};

export default Profile;
