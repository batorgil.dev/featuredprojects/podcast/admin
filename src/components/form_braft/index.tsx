import { ProFormItem } from "@ant-design/pro-form";
import { useRequest } from "ahooks";
import { message } from "antd";
import BraftEditor from "braft-editor";
import { FC, useEffect, useState } from "react";
import file from "services/user/file";

const FormBraft: FC<{
  name: string;
  initialValue?: string;
  disabled?: boolean;
}> = ({ name, initialValue, disabled = false }) => {
  const [data, setData] = useState<any>(BraftEditor.createEditorState(""));

  useEffect(() => {
    if (initialValue) {
      setData(BraftEditor.createEditorState(initialValue || ""));
    }
  }, [initialValue]);

  const uploads = useRequest(file.uploads, {
    manual: true,
    onError: (err) => message.error(err.message),
  });

  return (
    <ProFormItem name={name} noStyle valuePropName="data">
      <BraftEditor
        value={data}
        onChange={disabled ? () => {} : setData}
        media={{
          validateFn: () => {
            return true;
          },
          uploadFn: async ({ file: rawFile, success, error }) => {
            try {
              const res = await uploads.runAsync({
                files: [rawFile],
                names: ["image"],
                bucket_name: "content",
              });

              success({
                url: file.fileToUrl(res[0].path),
                meta: {
                  id: res[0].path,
                  alt: rawFile.name,
                  autoPlay: false,
                  controls: true,
                  loop: false,
                  poster: "",
                  title: rawFile.name,
                },
              });
            } catch (err: any) {
              error(err.message);
            }
          },
        }}
        style={{ border: "1px solid #d9d9d9", borderRadius: "6px" }}
        language="en"
      />
    </ProFormItem>
  );
};

export default FormBraft;
