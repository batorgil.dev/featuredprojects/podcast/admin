import { Avatar, Tooltip } from "antd";
import { FC } from "react";
import file from "services/user/file";
import { renderFullname, renderNameInitial } from "utils";

const UserAvatar: FC<{
  user?: {
    avatar_path?: string;
    first_name?: string;
    last_name?: string;
    position?: string;
  };
  haveName?: boolean;
}> = ({ user, haveName = true }) => {
  if (haveName) {
    return (
      <div
        style={{
          display: "flex",
          alignItems: "center",
        }}
      >
        <Avatar
          shape="circle"
          src={
            user?.avatar_path ? file.fileToUrl(user?.avatar_path) : undefined
          }
          style={{
            backgroundColor: "rgb(67, 95, 232)",
            cursor: "pointer",
            userSelect: "none",
          }}
        >
          {renderNameInitial(user)}
        </Avatar>
        <div style={{ marginLeft: 8, color: "#344054" }}>
          <div>
            <strong>{renderFullname(user)}</strong>
          </div>
          <div>{user?.position}</div>
        </div>
      </div>
    );
  }
  return (
    <Tooltip title={renderFullname(user)}>
      <Avatar
        shape="circle"
        src={user?.avatar_path ? file.fileToUrl(user?.avatar_path) : undefined}
        style={{
          backgroundColor: "rgb(67, 95, 232)",
          cursor: "pointer",
          userSelect: "none",
        }}
      >
        {renderNameInitial(user)}
      </Avatar>
    </Tooltip>
  );
};

export default UserAvatar;
