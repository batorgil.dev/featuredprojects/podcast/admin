import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import tsconfigPaths from "vite-tsconfig-paths";
import svgr from "vite-plugin-svgr";

// https://vitejs.dev/config/
export default defineConfig({
  esbuild: {
    jsxInject: `import React from 'react'`,
  },
  resolve: {
    alias: [{ find: /^~/, replacement: "" }],
  },
  css: {
    modules: {
      localsConvention: "camelCaseOnly",
    },
    preprocessorOptions: {
      less: {
        javascriptEnabled: true,
      },
    },
  },
  optimizeDeps: {
    include: ["@ant-design/icons"],
    esbuildOptions: {
      // Node.js global to browser globalThis
      define: {
        global: "globalThis",
      },
    },
  },
  server: {
    fs: {
      allow: ["."],
    },
  },
  build: {
    sourcemap: false,
    commonjsOptions: {
      ignoreTryCatch: (id) => id !== "stream",
    },
  },
  plugins: [react(), tsconfigPaths(), svgr()],
  define: {
    "process.env": {},
    "process.browser": true,
  },
});
